\section{Riot Control Agents}

\epigraph{
May we meet one day in a world without tear gas, in which skin color is not a weapon.
}{CrimethInc., \textit{What They Mean When They Say Peace}}

\noindent
In layperson's terms, riot control agents (RCA) are tear gas and pepper spray.
Because of how widely deployed they are, and the frustration at dealing with them, there are endless myths about these weapons from their mechanism of action to how to deal with contamination.
This section covers the basics of riot control agents, their treatments, and the urban legends themselves.
Protection against RCAs is covered in \fullref{sec:equipment}.

\subsection{Basics}

RCAs are lachrymators: they are agents that cause tear production in the eyes and otherwise irritate mucous membranes.
Generally, when someone says tear gas, they mean an RCA that is aerosolized into a cloud and deployed over a large area.
When they say pepper spray, they mean an RCA that is sprayed in a liquid form over a relatively short distance.
Most often, tear gas is CS gas and pepper spray is either oleoresin capsicum (OC) or PAVA suspended in a solvent.
There are some cases of DM gas being used which is an emetic agent (something that causes vomiting), and police or fascists may deploy other chemicals such as HC smoke (hexachloroethane) or pesticides outside their intended use as a means to cause discomfort and illness in protesters.

\begin{table}[htbp]
\caption{Common RCAs}
\centering
\begin{tabular}[c]{l l l}
	\hline
    OC   & oleoresin capsicum                    \\
    PAVA & pelargonic acid vanillylamide         \\
    CS   & 2-chlorobenzalmalononitrile           \\
    CN   & phenacyl chloride, chloroacetophenone \\
    CR   & dibenzoxazepine                       \\
    DM   & diphenylaminechlorarsine              \\
\end{tabular}
\end{table}

Tear gas is not a gas, and in some regions it is more aptly named tear smoke.
RCAs are deployed by aerosolizing the compounds.
This can be done by creating a micro-powder, dissolving it in a solvent, or simply burning it.

Tear gas is often deployed via a \SI{40}{\milli\meter} launcher that typically releases multiple projectiles or via grenades that explode or burn and release the agent.
Pepper spray is often deployed at close range using handheld devices.
Police may also  use large backpacks to soak protesters with large amounts of RCA.
Water cannons may mix RCAs in their streams to allow even larger scale deployment.

\begin{figure}[htbp]
\centering
\caption{Pepper Spray Delivery Devices\supercite{snailsnail}}
\includesvg[width=6cm, keepaspectratio]{pepper-spray-devices}
\end{figure}

\subsection{Signs and Symptoms}

Symptoms of exposure to all tear gasses (CS, CR, CN) are generally similar.
Under low concentrations, tear gas causes a burning sensation in mucous membranes, especially the eyes.
Other effects are tearing of the eyes, increased nasal mucus production, and coughing.
Moderate concentrations and longer exposure lead to profuse coughing, blepharospasm (involuntary closing of the eyelid), increased salivation, difficulty breathing (dyspnea), prostration (doubling over), burning and stinging sensations on the skin, disorientation, dizziness, syncope (fainting), headache, tachycardia, and vomiting.
Heavy concentrations, especially in enclosed spaces, can lead to death by asphyxiation or pulmonary edema.
Patients with preexisting respiratory disorders such as asthma are more sensitive to tear gas and exposure to even small quantities can be life-threatening.

Effects of pepper spray (OC, PAVA) are a burning like pain on the skin with severe pain in mucous membranes of the eyes, nose, throat, and lungs as well as increased nasal mucus production.
Even a small amount of pepper spray in the eyes causes blepharospasm.
Inhalation of pepper spray or residual vapors from contaminant on the face can cause coughing leading to prostration.
The most painful and severe effects of pepper spray typically abate in 15 to 30 minutes without treatment, though lingering eye watering and a sensation of burning of the skin may remain for 24 hours.
Like with tear gas, patients with preexisting respiratory disorders may have life-threatening closing of their airway.

It should also be noted that handheld devices do not necessarily contain OC or PAVA.
Either may be blended with other RCAs like CS, or the handheld device may only be CS.
Because of this, some of the debilitating effects associated with these chemicals such as dizziness and disorientation may be present.
Anecdotal evidence from patients suggests that they believe these debilitating effects are because of the ``strength'' and ``spiciness'' of the OC spray and the not addition of other RCAs.

\subsection{Treatment}

Treatment for all RCA contamination is generally similar and involves flushing the affected body parts with large amounts of water or saline to remove the agent.
During decontamination, prevent runoff from spreading the RCA to other parts of the patient's body or your body, especially mucous membranes or open wounds as this runoff can cause additional irritation.

Both syncope and vomiting are symptoms of exposure to tear gas.
Together these can lead to pulmonary aspiration followed by death via asphyxiation.
You should be on the lookout for patients who appear to be unresponsive when tear gas is deployed.

\triplesubsection{Remove the patient from the RCA}
If tear gas is used, the air may be noxious for many minutes.
Attempt to move the patient upwind from clouds of tear gas.
Tear gas is heavier than air, so if possible, move your patient to higher ground.

\triplesubsection{Prevent the patient from touching the affected area}
A patient's instinct will be to rub the affected body part, especially the eyes and face, while contaminated and after decontamination.
This can make the contamination worse and spread it to other body parts.
When RCAs are deployed, no one should touch their eyes at all except to remove contact lenses.

\triplesubsection{Remove contact lenses}
If the patient has RCA on their face or eyes, they should remove their contact lenses.
Flushing the eyes can push contact lenses up into the eye socket.
Ask your patient if they are wearing contact lenses, and if so, direct the patient to remove them before treatment.
If the patient cannot open their eyes or is incapable of removing the contacts, you may need to flush their eyes until they can open them to remove the contacts.

\quadruplesubsection{Clean contact lenses}
Some patients will attempt to save their contact lenses and reinsert them after you have decontaminated their eyes.
You should suggest they dispose of them immediately.
They may put the contacts due to impairment or other reasons, and your job is to help minimize recontamination and associated pain.
After treatment, assist the patient with cleaning their lenses.
Have them wash their hands using the solution from your bottle.
Then, have them them rub their contacts together between their finger and thumb as your slowly stream water onto the lenses for at least 30 seconds.
After they put their lenses back in their eyes, you may need to help them gently flush out residual RCA.

\triplesubsection{Remove contaminated clothing}
For heavy contamination, they may need to remove their clothes to prevent continued irritation.
Masks and bandanas need to be removed before decontaminating the face, but other clothing can be removed after.

\triplesubsection{Decontaminate the body part}
Flush the body part with large amounts of water.
Specifics techniques for decontaminating the eyes are covered later in this section.
Because pepper spray is oily, it may be useful to gently dab or wipe the affected area with gauze to remove the bulk of the pepper spray.
Vigorously rubbing and scrubbing will exacerbate the pain.

\triplesubsection{Rinse the patient's mouth}
Patients should rinse their mouth with water or saline to remove the RCA.
Even in the absence of burning or irritating sensations in the mouth, a mouth rinse is encouraged as it helps remove the taste and it helps them feel cleansed.

\triplesubsection{Allow coughing and sneezing}
If you patient is coughing or sneezing, allow them to continue as this is the body's natural response and it will help remove the RCA.
Give your patient tissue or gauze, and have them blow their nose.

\triplesubsection{Consider use of an inhaler}
If your patient is asthmatic, remind them to use their inhaler.
You may want to carry a salbutamol inhaler in your kit in the event patients have lost or forgotten theirs.

\triplesubsection{Consider treating for hypothermia}
Patients may remove contaminated clothing, and clothing may be wet from treatment.
On cool or breezy days, this can contribute to hypothermia.
Consider wrapping the patient in an emergency blanket so they do not have to put back on their contaminated clothes.

\triplesubsection{Consider other complications}
Patients may develop delayed respiratory distress or hyperventilation, or they may go into shock as their adrenaline wears off.
Consider treating them for respiratory distress.

\triplesubsection{Instruct the patient on how to decontaminate at home}
When you discharge the patient, direct them on how to safely decontaminate when they get home.

Clothing should be removed before entering their home.
Tear gas residue, especially CR, should be vacuumed off clothing and the body before entering the home.
The patient should throw out the vacuum bag after use to prevent spreading tear gas.

Clothing should be washed separately from other items, twice, and with a harsh detergent.
If clothing cannot be immediately washed, direct them to put it into a sealed plastic bag until they can wash it.

The patient should shower in a well ventilated room using the coldest water possible for at least 20 minutes.
Warm water opens pores and may cause additional burning sensations, so patients should shower with the coldest water they can tolerate until the feeling of burning stops.
Likewise, scrubbing affected areas should be avoided until burning stops.

\subsection{Eyewash Techniques}

For flushing the eyes, use a pneumatic eyewash bottle, contact solution bottle, or water bottle filled with water or saline.\footnotemark[1]
The patient should tilt their head forward to prevent pepper spray from running into an uncontaminated eye, the nose or mouth, or down their torso.
Whatever bottle you use, it should be held 2 to \SI{3}{\cm} from the patient's eye.

\footnotetext[1]{
Use of spray bottles, the type used for cleaning windows or misting plants, is not recommended.
In general, they do not allow the medic to spray a sufficiently high volume of water.
They are relatively ineffective on the mist setting, and the stream setting will usually startle the patient into closing their eyes.
Controlling the pressure of a stream from a spray bottle is difficult, and excessive spray pressure can damage the eye.
}

The main difficulty with flushing is the eyes is keeping them open either due to blepharospasm or the reaction to having water sprayed in them.
Always manually open their eyes with your fingers (\autoref{fig:rca_hold_open_eye}), though some may know to do this themselves.

\begin{figure}[htbp]
\centering
\caption{Opening a Patient's Eye\supercite{baedr}}
\begin{subfigure}[b]{5.3cm}
    \centering
    \includesvg[width=\textwidth, height=5cm, keepaspectratio]{open-patients-eye}
	\caption{One Medic}
    \label{fig:rca_hold_open_eye}
\end{subfigure}
\begin{subfigure}[b]{5.3cm}
    \centering
    \includesvg[width=\textwidth, height=5cm, keepaspectratio]{open-eye-two-medics}
	\caption{Two Medics}
    \label{fig:rca_hold_open_eye_two_medics}
\end{subfigure}
\end{figure}

When flushing the patient's eyes, have the patient tilt their head forward (\autoref{fig:use_of_pneumatic_bottle}).
Spray water or saline directly into their eyes one at a time.
Spray directly into their eye while directing the stream over the entire eye in small sweeping motions.
Each spray should only last 1 to 2 seconds.
Repeat as necessary.

\begin{figure}[htbp]
\centering
\caption{Decontamination with a Pneumatic Bottle\supercite{baedr}}
\label{fig:use_of_pneumatic_bottle}
\includesvg[height=6cm, keepaspectratio]{decontamination-with-pneumatic-bottle}
\end{figure}

\subsection{Multiple Patients and BSI}

A difficult aspect of treating patients who have been contaminated with RCA is that RCA contaminations, especially with tear gas, are short lived mass casualty incidents (MCI).
You may be rapidly overwhelmed with patients, and you will need to triage patients.
Patients who initially seem fine may degrade into respiratory distress or shock if they suffer an allergic reaction or their adrenaline wears off.

When treating multiple patients, as in all MCI situations, you need to direct all patients to come to you so you can monitor the status of all your patients simultaneously.
Find somewhere relatively safe to treat everyone where patients will not be trampled by a crowd or police charge.
This may simply be backing against the wall of a building or sitting patients on a planter.

If you are working with multiple medics, split responsibilities so that some medics treat RCA contaminations and some treat traumatic injuries.
To speed up treatment and prevent yourself from exhausting your supply of gloves, BSI best practices can be somewhat relaxed.
If patients are not bleeding and your gloves have not come into contact with their saliva or nasal mucus, you may reuse gloves between patients.

When treating multiple patients with RCA contamination, you may want to consider partially treating all patients first before making a second pass and doing a full decontamination.
Doing this allows you to triage patients and assess whether anyone needs additional interventions.
It is also beneficial to start with patients who have RCA in their eyes before moving to patients with RCA on their torso and extremities.

Doing an initial treatment on everyone before doing full decontaminations also reduces the total amount of panic in the group.
A partial rinse of the eyes may not stop the immediate feeling of burning, but it will reduce their pain levels, allow them to open their eyes some, and allow natural tear production to help flush out RCA.
Most importantly, partial treatment is comforting by showing that someone is there to care for them.

When you move to treat a new patient, make sure you clearly communicate that you are moving to the next patient and will be back.
This is especially true in panicked or blinded patients.
You do not want them to feel abandoned.

\subsection{Urban Legends}

Of all the topics within riot medicine, treatment for RCAs seems to be the most rife with misinformation and urban legends.
While commonly used as remedies for RCA contamination, none of the following should be used: liquid antacid and water (LAW), cow's milk, oil, citrus (lemon or lime), vinegar,\footnotemark[2] lidocaine, hydrogen peroxide, baking soda paste, toothpaste, and onion.
None of these show any efficacy for reducing pain, and in particular use of LAW can be dangerous.
Liquid antacid is not a liquid but a suspension of powdered antacid, and this grit can damage the patient's corneas.
Only use water or saline.

\footnotetext[2]{
Protesters and medics favor apple cider vinegar in particular.
It is equally as useless as all other vinegars.
}

\subsection{Secondary Injuries}

Shooting munitions at crowds and throwing burning or exploding grenades leads to secondary injuries.
Munitions can break bones, and if they're shot at head level, they can kill protesters.
Exploding tear gas cannisters can cause acoustic trauma and bits of shrapnel can cause other trauma.
Burning cannisters can cause full-thickness burns if they lodge in loose clothing.
The pressure from point-blank use of pepper spray can cause trauma to the eye.
Panicked crowds may run and trample each other.
Be aware of secondary injuries when RCAs are deployed.

\subsection{Summary}

Treatment of RCAs is simple: flush the affected area with water or saline until the burning has mostly subsided.
Be wary of hypothermia after decontamination or respiratory complications from the RCAs themselves.
