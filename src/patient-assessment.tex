\section{Patient Assessment}

\epigraph{
Revolutionary action is not a form of self-sacrifice, a grim dedication to doing whatever it takes to achieve a future world of freedom.
It is the defiant insistence on acting as if one is already free.
}{David Graeber, \textit{Possibilities}}

\noindent
Patient assessment does not need to be followed by rote, and the contents of this section are comprehensive to cover all cases.
Most injuries you will see are easily identifiable trauma, and clarifying questions are just to check for the possibility of head injury.
For everything else, the followng mnemonics and steps will help you quickly make a diagnosis or at least provide a starting point for treatment.

\subsection{Pre-Contact}

Before you reach your patient, size up the scene.
Check if it is safe for you to approach.
Is anything burning?
Are police charging?
Has a vehicle driven into a crowd, and is it disabled?
Do not become a casualty yourself.

The scene size-up will give you an idea of the mechanism of injury (MOI).
Is there a collapsed tripod?\footnotemark[1]
Is there an ongoing street fight between two armed groups?
Consider if the current scenario or environment could have caused an injury or illness.

\footnotetext[1]{
A tripod is a three legged structure that activists suspend themselves from to block roads or access to buildings.
Police must expend significant time to get them down safely.
}

\subsection{Primary Assessment}

Introduce yourself, and get consent before beginning treatment.
The primary assessment uses the mnemonic ABCDE to to remind the medic of the main steps of the process: airway, breathing, circulation, disability, and expose/examine injuries.
This assessment is for immediate threats to life, not about measuring quality or the nature of any vital signs.

\begin{table}[htbp]
\centering
\begin{tabular}[c]{l l}
    \hline
    \textbf{A} & Airway                  \\
    \textbf{B} & Breathing               \\
    \textbf{C} & Circulation             \\
    \textbf{D} & Disability              \\
    \textbf{E} & Expose/examine injuries \\
\end{tabular}
\end{table}

\begin{table}[htbp]
\centering
\begin{tabular}[c]{l l}
    \hline
    \textbf{M} & Massive hemorrhage \\
    \textbf{A} & Airway             \\
    \textbf{R} & Respiration        \\
    \textbf{C} & Circulation        \\
    \textbf{H} & Hypothermia        \\
\end{tabular}
\end{table}

If the patient is unconscious, check their airway and if they are breathing.
If not, begin CPR.\footnotemark[2]
Check for ``disabilities'' such as a broken neck and blood glucose levels.
Finally, check for hemorrhages by exposing or examining injuries.
While exposing and examining injuries, spend a few seconds to search the patient's entire body as the first injury you see may not be the most critical, especially in the case of gunfire.

\footnotetext[2]{
Some texts recommend not check for a pulse as it may be weak or difficult to find in a chaotic setting.
Respiratory arrest is followed promptly by cardiac arrest, so there is a bias to beginning CPR rather than checking for a pulse.
If you are confident in your abilities, you may choose to check for a pulse during respiratory arrest and use its presence/absence for guiding basic life support.
}

In combat scenarios, ranging from knife fights to use of live ammunition, the mnemonics MARCH is often used to address greatest threats to life first.
MARCH stands for for massive hemorrhage, airway, respiration, circulation, and hypothermia.
Hemorrhages need to be controlled and the airway needs to be cleared before beginning CPR.
Patients may cool rapidly even in warm weather and need to be kept warm, hence the ``H.''

\subsection{Secondary Assessment}

Check the patient's heart rate, respiratory rate, blood pressure, peripheral blood oxygen saturation, blood sugar, and temperature.
For manual measurement of heart or respiratory rates, measure for at least 15 seconds and multiply by 4 to get an accurate measurement.
Axillary temperature is sufficiently accurate for measuring core temperature.
If possible, write all measurements in a notebook along with the current time.
This can help check for improving or worsening conditions, and it may be relevant to the next healthcare provider.

If the patient has no immediately life threatening conditions, perform a head-to-toe examination.
Start at the head and palpate their body while looking for deformities or tenderness.

If the MOI suggests significant trauma, do a rapid trauma assessment to look for things like CSF leaks or internal bleeding.
Use DCAP-BTLS mnemonic during your quick, 60-90 second assessment.
If you find nothing, go on to a more thorough secondary assessment.

\begin{table}[h]
\centering
\begin{tabular}[c]{c l}
    \hline
    \textbf{D} & Deformities  \\
    \textbf{C} & Contusions   \\
    \textbf{A} & Abrasions    \\
    \textbf{P} & Penetrations \\
    \textbf{B} & Burns        \\
    \textbf{T} & Tenderness   \\
    \textbf{L} & Lacerations  \\
    \textbf{S} & Swelling     \\
\end{tabular}
\end{table}

Check the overall quality of the patient's skin using the mnemonic SCTM.
Human skin naturally has great degrees of variation in color and even among patients with apparently similar skin color, there are different undertones (e.g., a person with ``white'' skin may have red or olive undertones).
When you are examining skin color, you are not taking an absolute measurement, especially when looking for paleness (pallor) or redness (erythema).
You need to compare their current color against the their baseline color.
If this is difficult to asses, you may choose to hold up a small mirror so the patient can see their face and ask ``Is this your normal skin color?''

\begin{table}[h]
\centering
\begin{tabular}[c]{c l}
    \hline
    \textbf{SC} & Skin Color \\
    \textbf{T} & Temperature \\
    \textbf{M} & Moisture    \\
\end{tabular}
\end{table}

In unconscious patients, prior to beginning diagnosis, you would have attempted to wake them with verbal or painful stimuli.
This would give you their partial GCS.
Fully asses their GCS during the secondary assessment.
Check for orientation by asking them if they know their name, the date, the approximate time, and the events the lead to their injury illness.
Asking their name may be dangerous, so you can phrase the question as ``Don't say it aloud, but do you know your name?''
To check for anterograde amnesia, ask them to remember a simple word like ``avocado,'' and ask them to repeat it later.

\begin{table}[htbp]
\centering
\begin{tabular}[c]{c l}
    \hline
    \textbf{C} & Circulation \\
    \textbf{S} & Sensation   \\
    \textbf{M} & Motion      \\
\end{tabular}
\end{table}

When checking the extremities, check for damage to nerves and blood vessels using CSM.
Check for the normal capillary refill time of under 2 seconds by squeeing a fingernail or toenail for 3 seconds and releasing.
Check for parathesia (numbness, itching, or tingling) in their fingers and toes.
Check for motion and strength in their hands and feet by having them push and pull against your hands.
When checking for motion, avoid commenting on a lack of movement as this may distress the patient.

You may need to get the patient's recent medical history, in which case use the SAMPLE mnemonic.
When investigating their chief complaint, OPQRST can help you quickly converge in on the problem.
These are relatively self-explanatory.

\begin{table}[htbp]
\centering
\begin{tabular}[c]{c l}
    \hline
    \textbf{S} & Signs and symptoms         \\
    \textbf{A} & Allergies                  \\
    \textbf{M} & Medications                \\
    \textbf{P} & Past medical history       \\
    \textbf{L} & Last oral intake           \\
    \textbf{E} & Events leading to incident \\
\end{tabular}
\end{table}

\begin{table}[htbp]
\centering
\begin{tabular}[c]{c l}
    \hline
    \textbf{O} & Onset              \\
    \textbf{P} & Provokes/palliates \\
    \textbf{Q} & Quality            \\
    \textbf{R} & Region/radiation   \\
    \textbf{S} & Severity           \\
    \textbf{T} & Time               \\
\end{tabular}
\end{table}

If the patient has a suspected spinal injury, you may be able to clear them yourself during the secondary assessment.
The criteria for clearing a patient using the focused spine assessment are as follows:

\begin{enumerate}
    \item The patient is reliable.
        \begin{enumerate}
            \item They have a GCS of 15.
            \item They are unintoxicated.
            \item They are not distracted either by preoccupation, onlookers, or rioting happening around them.
            \item They are not distracted by another injury such as a broken arm or leg.
        \end{enumerate}
    \item The patient has normal CSM in all four extremities.
        \begin{enumerate}
            \item Their skin is warm and pink, and they have radial/pedal pulses.
            \item They have normal sensation and no tingling or numbness.
            \item They can move their hands/feet and have strength unless a lack thereof can be explained by another injury.
        \end{enumerate}
    \item Their entire spine is free of pain and tenderness when palpated.
\end{enumerate}

To palpate the spine, you need to have direct access to the spine from the base of the skull to the bottom of the back.
With at least two other medics assisting, roll the patient on to their side while stabilizing their cervical spine.
Palpate the spine starting from the base of the skull and working towards the pelvis.
This cannot be performed over bulky clothing.
Each vertebra should be individually palpated.
If the palpation is painful, the patient may have a broken vertebra.
Stop the examination and roll the patient on to their back.
Evacuate the patient to advanced medical care.

\subsection{Triage}

True mass casualty incidents (MCI) are generally rare at riots, so here we look a more narrow scope of an MCI: anything that overwhelms you and your team.
Often this is a police charge, use of riot control agents, or multiple concussions because of opposition throwing bottles and stones.
While normal triage principles apply, be aware of a few things:

\begin{enumerate}
\item Even small head wounds bleed a lot making blood a potentially poor indicator of severity of injury.
\item Experienced protesters are accustomed to violence and may be calm while severely injured.
\item Inexperienced protesters are not accustomed to violence and may be panicked after taking a baton to the thigh or being lightly tear gassed.
\end{enumerate}

If you have multiple patients, their subjective assessments and insistent friends may be inaccurate measures of the severity of their injuries.
Pause to assess things yourself and make a decision on who to treat after a very quick (10-15 second) assessment.
Delegating someone to tend to the panicked can reduce load.

\subsection{Calling EMS and Handover}

Calling EMS or working directly with them may be part of your common duties.
If not, the following are useful for quickly and efficiently communicating information to EMS.

\begin{itemize}
    \item In one short sentence, state the reason why you are calling EMS.
          This may be  as simple as ``I have an unconscious patient with head trauma.''
    \item Give your exact location.
          This may be a cross street, address, or position relative to a landmark.
    \item Give the dispatcher your mobile phone number so that they may call you back if the call is disconnected.
    \item State your qualifications. They may be able to guide you through treatment.
    \item Give relevant details on the patients and their injuries that may allow the dispatcher to send the appropriate resources or to prioritize your patients against other incidents.
    \item Do not hang up until directed to do so.
          The dispatcher may instruct you to stay on the call until an ambulance arrives in order to help guide them to you.
    \item Expect delays.
          EMS response may be with 5 minutes normally, but due to blockades, it could be hours, if at all.
    \item Expect police.
          They may arrive with EMS.
\end{itemize}

Handover to EMS generally goes smoothly, especially if one medic on the team works in healthcare.
State your qualifications, and quickly give them your notes.
If your buddy does not work in healthcare and they are the ones who meet EMS to lead them to you, have them state your qualifications so they will treat you as a peer when they arrive.

\subsection{Summary}

Patient assessment is done in two steps.
First, check for life threatening conditions using ABCDE or MARCH.
Attend to these, or if they're not present, move on to a secondary assessment.
Take vitals signs like heart rate, respiratory rate, blood pressure, GCS, blood sugar, oxygen saturation, and temperature.
Mark these down for reference later.

% just to help with formatting
\clearpage
