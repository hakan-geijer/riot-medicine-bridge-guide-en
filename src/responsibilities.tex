\section{Responsibilities}

\epigraph{
Nothing every burns down by itself. \\
Every fire needs a little bit of help.
}{Chumbawamba, \textit{Give the Anarchist a Cigarette}}

\noindent
Riot medics make up the autonomous part of the healthcare system that attends actions and interfaces with the organizations and individuals involved in protest and civil unrest.
They may seem to exist wholly outside of the classic healthcare system, but they are organized, trained, and fulfill the same role, albeit at a smaller scale and with far fewer resources.
They help educate their community on matters of health and provide emergency medical care.
What separates the autonomous system from the classic system is that the former is able to be on the ground, in numbers, continuously, and it can do so where the latter cannot go.

\subsection{Basics}

The most obvious examples of a riot medic's responsibilities are first aid, evacuation, and transfer of care to Emergency Medical Services (EMS).
Their responsibilities can also be as mundane as handing out water on hot days or emergency blankets on cold days.
Like classic medicine, they also work to prevent injuries and illness via education about the kinds of things seen during protest or generally while engaged in physical activity.
Typically, their tasks at actions include:

\begin{itemize}
    \item Providing care to injured individuals
    \item Spreading calm when others may panic
    \item Evacuating injured persons to safety
    \item Interfacing with classic EMS
    \item Setting up makeshift clinics to care for multiple casualties
    \item Instructing patients on aftercare
    \item Providing emotional and psychological support for patients experiencing trauma
\end{itemize}

Your patients are likely to be psychologically traumatized when they are injured.
They may have never experienced police brutality before, or they be psychologically strained after months or years of repeated brutalizations.
It is more important than anything else to be a beacon of calm amidst panic and chaos.
Be kind and gentle.
Emotionally care for your patients.
Give them assurances that someone is there to help.

Medics may also be involved in jail support to help protesters post-arrest.
There may be a mix of legal support, medical support, and others with clean clothes and food who wait outside jails to care for people after they've been booked.
Talk to local organizations and see if this is something you can help with.

All services you offer must be free of charge.
Do not solicit donations from patients as this may be interpreted as a requirement for an ``unofficial'' payment.
Not everyone can pay, and putting up even this small barrier makes treatment inaccessible to some patients.
Most importantly, it breaks down the solidarity we are trying to build with one another.

\subsection{Non-Prevention}

Part of medicine is preventing injury and illness, and this applies to riot medicine.
Medics should educate about risks like dehydration, hypothermia, and tear gas.
However, medics should generally not attempt prevent injury by intervening at action.\footnotemark[1]
The success of an action is very often more important than preventing injury, and intervention is paternalistic.
It assumes that the person intervening knows better than the person acting on their on volition what is good or desirable for them.
Individuals have the autonomy to make choice on the activities they want to engage in, and your role is about helping minimize unwanted consequences of these choices rather that restricting the set of choices anyone can take.
Those who take such preventative actions are often dubbed the Peace Police and shunned at actions.

\footnotetext[1]{
Exceptions are of course for things that are extremely likely to result in severe injury or certain arrest with little chances of anyone's goals being achieved.
However, if you are new to radical actions, you probably should just watch and learn.
}

\subsection{Patient Consent}

The treatment of every patient must begin with informed consent.
Introduce yourself and state your qualifications, often just as simply as ``Hello. I'm a doctor/nurse/paramedic. Can I help you?''
Ask them about their chief complaint, explain what you plan to do, and reaffirm consent as necessary.
Pause to allow the patient to reject treatments, and take care not to push them to accept any treatment.

Due to the more chaotic setting where you will find and treat patients, and due to the limited amount of equipment you will be able to carry, the informed part of informed consent can be abridged because the initial treatments are conservative.
You as the provider need to select treatments that are unlikely to not be fully consented to or could be confusing to the patient.
Be aware that you may have relative privilege over your patients either due to your race, gender, stature, or more official looking uniform.
They may not fully consent but go along with your treatments because they feel intimidated or see you as the only option.

Like how there is implied consent for patients with altered consciousness, protesters who are disoriented or blinded by riot control agents implicitly consent to extraction.
Alert them that you're not a threat.
Shout ``I'm a medic. Let's go!''
Lack of obvious, immediate dissent should be taken as assent.\footnotemark[2]
Put an arm around them to offer stability, and guide them to safety.

\footnotetext[2]{
As an anecdote, in all my years of medic work, I have literally never had a patient resist this sort of quick extraction, and all have been glad I was able to snatch them away from danger.
}

Patients may often only want a bare minimum of treatment so they can return to a fight.
You may feel that a laceration requires sutures, but they may accept the risk of damage and scarring and ask for just wound closure strips and gauze to wrap it.
Appropriately warn them, and do what you can to convince them to seek care later, but still give them quick treatment as that is better than nothing.

When a patient consents to care, they are only consenting to the care necessary to heal them or provide lifesaving interventions.
They are not consenting to a full medical examination or the collecting of tissue samples.
Asking to take samples is likely coercion.

For example, during Yellow Vest actions in France in 2019, there were reports on April 20th and May 1st that street medics were taking blood samples.\supercite{paris-medics-blood-samples}
These medics claimed to be doctors who wanted to analyze the blood for hydrogen cyanide from expired CS gas.
Consent was given, but not freely, while injured persons were treated on the streets.
Photos taken of these events show medics wearing respirators and patients attempting to cover their faces because they were still in danger.
The medics chose to treat the patients as lab rats to satisfy their own curiosity instead of getting the patients to safety.
Do not do this.

\subsection{Patient Confidentiality}

Patients have the right to privacy, and it may be illegal to share patient data, even with other healthcare providers without explicit consent.
During protests and uprisings, these are not just rights but requirements for patient safety.
Police repression can lead to extrajudicial retribution against protests during or after actions.
You may be tempted to document your work to avoid lawsuits, but practitioners are generally covered by Good Samaritan laws.
Documentation can be confiscated and used in court.

Do not collect any patient data.
Do not take photos.
Do not ask for names, and if you do ask for a first name only.
Phrase such queries as ``Is there a name I can call you?'' to make it clear that you do not care about their legal name.
Do not ask for ID cards or insurance cards.
Do not talk to the police, or even around the police, about patients.
If they ask, respond with ``I legally cannot discuss my patient with you.''
If they try to film or observe, push them back and tell them they are violating patient confidentiality.\footnotemark[3]

\footnotetext[3]{
This works better during actions that are less riotous and in regions where the police act more in line with the so-called neutrality that liberal democracy professes.
}

\subsection{Patient Abandonment}

Patient abandonment occurs when there is a negligent termination care on by the caregiver or failure to handover care to another qualified provider.
Caregivers may terminate care for valid reasons such as:

\begin{itemize}
    \item The scene becomes unsafe for the medic
    \item The patient assaults or harasses the medic
    \item The medic is incapable of providing the care the patient needs
    \item Ethical or legal problems would arise as a result of further treatment
\end{itemize}

Most treatments are short, and patient abandonment does not meaningfully apply.
However, even non-life-threatening injuries or illness may require hours of care.
It is common for ambulances to be delayed because of barricades, police blockades, or orders to not enter dangerous environments.
You may need to spend significant time monitoring and attending to a single patient until EMS arrives.
This is often complicated by the fact that there may still be rioting near you.
Others may be injured, and people may try to pull you away from your patient to treat their comrade.
Unless the new patient is described as having imminent risk of death, do not leave your current patient.
Have people bring all new patients to you so you can rotate between them.

If can feel like you're useless when there is rioting happening around and you're waiting with a stable patient, but leaving them to try to extract someone is never as simple as it looks.
You can get snatched, leaving your patient alone.
Rely on others to bring patients to you.

\subsection{EMS, the Police, and Harm Reduction}

EMS may voluntarily or compulsorily work with the police.
Police may station officers at hospitals with the expectation that persons injured during riots will seek medical care.
In general, and in particular at riots, calling EMS may cause the police to arrive.

Always get consent from a patient before calling EMS.
They may know the risks better than you, and if they do not, you may need to inform them that police may arrive or visit them at the hospital.
If they are not of sound mind, implied consent gives you grounds to call EMS.

Because advanced medical care may not be possible, you may need to provide them the definitive care they need.
This may mean irrigating a wound and taping it shut or sometimes just instructing them on how to further debride a wound at home with cuticle scissors.
A hospital may be the ideal treatment, but they may not be able to go, and the small amount of harm caused by you cleaning a wound or reducing a dislocated finger is still a reduction compared to arrest or no treatment at all.

\subsection{Risk}

Confronting the State and fascism is an inherently risky endeavor, and there is some amount of risk you need to accept by joining a social movement.
Some people chose to take on the role of medic instead of ``just'' being a protester because they believe in its relative safety.\footnotemark[4]
Some do this out of fear, and others do it for a bit of the thrill.
They want to get close to action and have stories to tell without as much risk of being arrested.

\footnotetext[4]{
In some regions, medics in uniform may experience less violence from the police.
In some regions, they experience more.
}

Ask yourself honestly why you are becoming a medic.
If you are doing it for the thrill, you can endanger and kill patients.
If committed medics see you, they may move elsewhere with the expectation that you will provide care and not turn away when things get violent.
If you are treating a patient, and a police officer tells you to stop, stopping can kill the patient.
You should be willing to face arrest to continue treating seriously injured patients.

For example, during Unite the Right in Charlottesville, Virginia on August 12th, 2017, a neo-nazi drove a car into antifascist protesters injuring, among others, Heather Heyer.
Street medics assisted Heyer and performed CPR when she went into cardiac arrest.
A State Trooper forcibly removed an EMT from assisting, threatened others who lined up to help, and attempted to stop a street medic (a nurse) from performing CPR.\supercite{a12-medic-igd, a12-medic-tweet}
Heyer died as a result of her injuries.
This is not the only documented case, but it is one of the most prominent.

Generally, you should know what laws you are willing to break in the course of your work.
This may mean simply remaining with a protest that is declared an unlawful assembly, or it may mean working closely with insurrectionaries during fighting in the streets.
You should figure out what actions you are or are not willing to take or what laws you are willing to break before you are confronted with the decision.
This allows you to have a sense of risk beforehand so you don't make irrational snap decisions you may later regret.

\subsection{Self-Preservation}

As a medic, you need to first and foremost care for yourself.
If you become a casualty, you and your team will no longer be able to provide medical support, and another team will have to care for you.
Medics are limited, and your one injury can take a total of four medics out of action for hours.

Activists, including riot medics, may develop unhealthy complexes that push them into engaging in unnecessarily risky behavior.
Some people develop martyr complexes where they feel they aren't really contributing toward a cause unless they are suffering.
This may come from external drivers like the need to ``prove'' to others that they are truly dedicated to a cause or the need to demonstrate credibility by getting injured or arrested.
The best way to demonstrate your dedication to a cause is by repeatedly showing up to actions and efficiently rendering care.

\subsection{Neutrality}

Many people who enter the medical field, and especially those with the kinds of sympathies that would cause them to become a riot medic, do so because they believe in helping others and that all deserve medicine.
When you become a riot medic, your primary group you are responsible for care for are protesters and bystanders, not their opposition or the police.
Police have their own medics and most have basic first aid training.
They often have much better armor than protesters, and they are able to receive care that may be inaccessible to protesters.

Treating police or fascists can be dangerous.
Medics get attacked by police and fascists while treating them.\footnotemark[5]
Fascists will attack medics unprovoked and may see them as the enemy.

\footnotetext[5]{
In 2020 during the George Floyd uprising, street medic Sierra Boyne and her buddy attempted to treat a right-wing casualty.\supercite{portland-reinoehl-medic}
She was pushed off by both the victim's affiliates and the police when they arrived.
}

You may feel you have an obligation to help anyone who is injured, but remember that any time spent helping police or fascists is time not spent helping protesters.
There are often not enough medics for the volume and severity of injuries at large mobilizations.
What you consider neutrality is actually picking a side.

\subsection{Summary}

The main responsibilities of a riot medic are treating patients and evacuation to advanced medical care.
Major differences are that ambulances may be delayed or blocked, and that you may be stuck with one patient for hours.
Your roll is not about preventing actions, but dealing with consequences; let people take radical actions.
Consent may need to be abridged, and treatments may need to be minimal.
Protect patients by not collecting information.
Protect yourself by not being excessively risky, but don't be so careful that you never actually help anyone.
Do not abandon your patient to go in search of others.

Above all else, be the beacon of calm that allows people to feel safe and secure.
