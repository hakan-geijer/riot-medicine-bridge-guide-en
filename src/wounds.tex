\section{Wounds}

\epigraph{
Without a doubt, patriotism and militarism constitute the foundation upon which the imperialism practiced by the great powers of the present day rests.
}{Shūsui Kōtoku and Uchimura Kanzō, \textit{Imperialism}}

\noindent
Basic wound management is to clean, irrigate, and dress the wound.
This is hard to do wrong, so this section only covers slightly more complex wounds that may go beyond your normal experiences.

\subsection{Controlling Bleeding}

Direct pressure is the primary method of controlling bleeding.
Large wounds may need to be packed with gauze before applying pressure.
Abdominal wounds or wounds with obvious arterial bleeding should be packed with hemostatic gauze.\footnotemark[1]

\footnotetext[1]{
Hemostatic granules are harder to use.
}

For wounds to extremities, create a pressure dressing (\autoref{fig:pressure_dressing}) by placing a bulky dressing (a thick gauze pad or a rolled up gauze roll) directly over the wound.
Use a gauze roll or folded triangle bandage wrapped around the extremity to secure the bulky dressing in place.
Ensure that when wrapping the extremity, the bulky dressing is fully covered so it cannot slip out.
Tie a knot directly over the wound.
Check the patient's CSM to ensure adequate perfusion.

\begin{figure}[htbp]
\centering
\caption{Pressure Dressing\supercite{bizhan}}
\label{fig:pressure_dressing}
\includesvg[width=\textwidth, height=5cm, keepaspectratio]{pressure-dressing}
\end{figure}

If pressure dressings are insufficient, use of a tourniquet is recommended.
Early use of tourniquets is associated with improved survival rates with minimal risk.\supercite{tourniquet-battle, tourniquet-practical, tourniquet-survival}
These should still be used as a last resort as once they are applied, they should remain in place until the patient reaches definitive care due to risk of rhabdomyolysis and renal failure.
Applying a tourniquet may force a patient to go to the hospital, and if there is significant repression, this can lead to them getting imprisoned or disappeared.

\subsection{Wound Closure}

Patients may not be able to go to a hospital either on financial grounds or due to repression.
Carrying wound closure strips allows your to close wounds in the field that would otherwise be left open.

\begin{figure}[htbp]
\centering
\caption{Wound Closure Strip Application\supercite{bizhan}}
\label{fig:wound_closure_strip_application}
\includesvg[width=\textwidth, height=6cm, keepaspectratio]{wound-closure-strip-application}
\end{figure}

To apply wound closure strips, first clean and dry the area surrounding the wound.
Remove one strip from the package and press it on to the skin on one half of the wound.
Using your other hand, pinch the wound together to close it, then press the remaining half of the strip against the skin on the other side of the wound.
Every \SI{3}{\mm}, repeat this process to fully close the wound.
Optionally, after closing the wound, place additional strips parallel to the wound to reduce tension.
If possible, cover with Tegaderm or similar transparent film dressing; this will help secure the wound closure strips in place, while still allowing visual inspection of the wound.

\subsection{Open Abdominal Wounds}

Treatment is to first control the bleeding.
Do not remove objects protruding from the wound or organs.
Do not remove clothing that is stuck to the organs.
Cut the rest of the clothing away leaving the clothing in contact with the organ in place.
Use plain or hemostatic gauze to stop any bleeding.

Once bleeding is controlled, do not attempt to put the organs back into the body.
If you must handle the organs, do so very gently.
Cover the organs with gauze moistened with saline or water.

\begin{figure}[htbp]
\centering
\caption{Open Abdominal Wound\supercite{baedr}}
\label{fig:open_abdominal_wound}
\includesvg[width=\textwidth, height=4cm, keepaspectratio]{open-abdominal-wound-dressed}
\end{figure}

To reduce tension on the abdomen, place the patient's feet towards their pelvis so their knees are elevated (\autoref{fig:open_abdominal_wound}).
Treat for shock.
Evacuate the patient to advanced medical care.

\subsection{Chest Wounds}

Chest wounds can cause pneumothoraces.
Application of a vented chest seal can prevent air from entering while allowing air to exit.
This can help prevent a tension pneumothorax.
Note that a vented chest seal is specified.
A non-vented chest seal can trap air in the pleural space either causing or exacerbating a tension pneumothorax.

\triplesubsection{Commercial vented chest seal}
Commercial chest seals use extremely sticky adhesive that adheres to wet skin.
Be cautious of letting the seal come in contact with itself or your gloves.

Use trauma shears to cut away the patient's clothing.
Attempt to use gauze to dry to the skin around the wound to maximize adhesion between the skin and the chest seal.
Remove the seal from it's packaging.
Place the vent directly over the wound (\autoref{fig:application_vented_chest_seal}).
If the there is both an entrance and exit wound, place the vented seal on the anterior wound to allow it to vent.
The posterior wound may use a non-vented chest steal so long as a vented seal was used on the anterior wound.
Press the seal tight against the skin to ensure an airtight seal.

\begin{figure}[tbp]
\centering
\caption{Application of a Vented Chest Seal\supercite{baedr}}
\label{fig:application_vented_chest_seal}
\includesvg[width=6cm, keepaspectratio]{vented-chest-seal}
\end{figure}

\triplesubsection{Improvised occlusive dressing}
A occlusive dressing can be improvised.
Use a square piece of a plastic bag to cover the wound.
Tape down three of the four sides to allow air to escape.

\subsection{Dog Bites}

Police may have canine detachments with attack dogs that are used to intimidate protesters and subdue individuals for arrest.
Dogs are often trained to bite once and hold, but individuals may have multiple bites.
Dog bites can lead to puncture wounds, lacerations, crushed bones, and damaged muscles, tendons, and nerves.
The bacteria in a dog's mouth can lead to infection including MRSA.

Treatment for dog bites similar to that of other lacerations or puncture wounds.
Wounds should be cleaned with water and a mild antibacterial soap then thoroughly irrigated.
A difference to other wounds is that not all bite wounds should not be initially closed.\supercite{msd-bites}
Bites that are appropriate to close are lacerations on the face and scalp, wounds that do not extend into subcutaneous tissue, wounds with simple characteristics, wounds without underlying fracture.\supercite[pp. 317]{tintinallis}
Additionally, wounds should not be closed in immunocompromised patients.

\subsection{Handcuff Injuries}

% TODO include image showing areas of hand typically affected by compression of the radial nerve

Handcuff neuropathy is neuropathy of the hand caused by compression of the superficial branch of the radial nerve.
This is the most commonly affected nerve, but other nerves of the wrist may be affected.\supercite{handcuff-complaints}
Handcuffs may become over tightened during struggle or as a means of control.
Metal handcuffs come with a double-locking mechanism to prevent additional tightening after they have been locked, but police may forget or ``forget'' to do this.
Plastic handcuffs are often used for mass arrest.
Some models lack double-locking mechanisms which allow additional tightening.

\triplesubsection{Signs and Symptoms}
Signs and symptoms of handcuff neuropathy include tingling or numbness (parathesia) and weakness.

\triplesubsection{Treatment}
Remove watches, jewelry, or anything else that may constrict the patient's wrist.
It is unclear if edema contributes to handcuff neuropathy or if it is caused by compression alone,\supercite{tunnel-syndromes-ch26} and as such NSAIDs are commonly prescribed.
Consider administering NSAIDs.
Apply cold compresses to reduce pain and swelling.
The patient may need follow up from a physician, and symptoms may persist for months regardless.

% to help with formatting
\clearpage
