\section{Organizing}

\epigraph{
I will not rule.
I will not be ruled.
}{Anarchist aphorism}

\noindent
Riot medics are often organized into collectives or loose affiliations of individuals within a region.
Some groups may have a high number of members who are physicians, nurses, and the whole range of paramedics.
Others may be predominantly, or even exclusively, individuals with only first aid training.
Using the what knowledge and resources they have, they provide assistance to protesters at demonstrations and riots.

\begin{figure}[htb]
\centering
\caption{Training Together\supercite{baedr}}
\includesvg[width=0.6\textwidth, keepaspectratio]{medics-training-together}
\end{figure}

\subsection{Groups and Collectives}

Liberatory social movements aim to remove hierarchies and to create a world based on cooperation and mutual aid.
These movements are often prefigurative: they try to live and organize in a way that reflects the desired future state they are striving for.
Ergo, they reject existing hierarchies and ways of thinking even if one could argue such things are more effective.\footnotemark[1]
The kinds of decision making processes and chains of command you may be used to in life, and in particular in your workplace, will not exist.
You may need to learn how to participate in collective decision making and how to reach consensus.
You may need to learn how to operate autonomously while also leaving space for the autonomous actions of others.

\footnotetext[1]{
I say ``could argue,'' but in reality centralized, top-down organizations are not actually more effective than decentralized, bottom-up organizations.
}

Hierarchy in this sense refers (loosely) to ranked social relationships where some have the authority to give orders to others and punish those who disobey.
A simple example is that of the employer-employee relationship where employee who disobeys their employer can be fired.
Another example is a union or club that has a president or councilors that can set group policy and revoke membership of those who didn't follow it.
Non-hierarchical organizing does away with these sorts of coercive relationships, and it exists based on voluntary association.
Individuals participate because they choose to, and no one in the group has power over anyone else.

Some groups use a stronger consensus model where they attempt to get (close to) 100\% agreement before making a decision.
Some groups use a weaker model where a decision is made when everyone can live with the decision even if they're not happy.
Some groups have loose affiliations and no consensus is reached, and they only come together to share information and coordinate knowing that some will take action that others completely disagree with.
Consensus is not simply majority rule where 51\% of members can trample the other 49\%.
Care is taken for everyone's desire and autonomy, and when care is not taken or if a member vehemently disagrees, they are free to leave.

This non-hierarchical approach to organizing does not disregard the expertise within medicine.
A physician likely has more knowledge of human physiology than a paramedic, and an acknowledgement of these differing qualifications during treatment is still expected.
Likewise, a medic with no certifications likely knows more about how to move and act during a protest than medical professionals new to riot medicine.
There is a long tradition of this within radical thought.
For example Mikhail Bakunin wrote of the following on expertise in 1871\supercite{god-and-the-state}:

\begin{displayquote}
In the matter of boots, I refer to the authority of the bootmaker; concerning houses, canals, or railroads, I consult that of the architect or engineer.
For such or such special knowledge I apply to such or such a savant.
But I allow neither the bootmaker nor the architect nor the savant to impose his authority upon me.
\end{displayquote}

For you, this means that your experiences as a paramedic or your knowledge as a doctor will be deferred to when necessary, but your experiences, knowledge, age, or other traditional markers for seniority do not grant you the right to make demands of others.
You may be the only person in a collective who has any formal medical training, but this does not make you either a \textit{de jure} or a \textit{de facto} leader.

You may be treated like an ``outsider'' as your are a new face and do not come from a radical background, and it may be uncomfortable.
Because of this, you may be tempted to take to the streets on your own, but it is highly recommended that you try to work with established collectives with significant experience.
They have skills from being on the ground that you likely do not, and they know what danger exists at protests and how to avoid it.
High level medical knowledge is not sufficient to make someone a riot medic.
An experienced protester with only knowledge of first aid is near universally a better medic than a physician who does not know how to handle themself at a protest.

\subsection{The Buddy Pair}

The buddy pair is the fundamental organizational unit for medics.
If you work in an ambulance, you are likely very familiar with this concept.
You and your buddy are responsible for looking after each other's well-being (both physical and mental) before, during, and after an action.
A medic does not separate from their buddy during an action with the exception of multiple patients in close proximity requiring care.
You cannot take care of your buddy, and they cannot watch out for you when you are not together.
Separating endangers both parties.

\begin{figure}[htb]
\centering
\caption{Buddies\supercite{baedr}}
\includesvg[height=6cm, keepaspectratio]{buddy-pair}
\end{figure}

Some common responsibilities you and your buddy have toward each other include:

\begin{itemize}
    \item Being a second set of eyes while scanning for patients or danger
    \item Providing a second perspective or opinion for a situation (medical or otherwise)
    \item Being the devil's advocate or voice of caution
    \item Dividing equipment and carrying redundant equipment
    \item Double checking equipment
    \item Reminding the other to eat and drink
    \item Being alert while the other rests
    \item Controlling a crowd while the other assists a patient
    \item Preventing people from taking photos or videos of a patient
    \item Being a secondary for two-person CPR
    \item Assisting with moving or carrying a patient
    \item Communicating with EMS, other medics, or other groups while the other is helping a patient
    \item Debriefing each other at the end of the day
    \item Supporting each other's mental health in the long-term
\end{itemize}

Ideally, your buddy would be another medic, but if that is not possible, a friend or comrade is a suitable alternative.
They can still hand you supplies and be watchful eyes while you attend to a patient.
If they have protest experience, the can be your guide and keep you out of unnecessary danger.

\subsection{Contingency Planning}

Even if protest is legal and rather safe in your region, the risk of arrest or serious injury is never zero.
Far-right ``lone wolves'' may violently attack your protest, or police may decide they just don't like your attitude and arrest you, possibly landing you in jail.
Even as a medic, where these risks may be lower than the average protester, you still face some risk of imprisonment or serious injury.
If you're unlucky, police will heavy-handedly target medics with greater ferocity than other protesters.\footnotemark[2]
Because of these risks, you should make and share with comrades your contingency plans for what to do if you are injured, are arrested, or have your house raided by the police.

\footnotetext[2]{
Medics in the US are specifically targeted by the police for violence and arrest.
}

Consider talking to a lawyer who specializes in social movements and putting them on retainer.
If you cannot afford this, consider finding a lawyer who may take on many clients from the same collective for a group fee.
There may be non-profit legal assistance for people arrested during protests in your area.

If you have pets, give a friend a key to your home, and tell them how to care for your pets.
If your pets have special diets or medication, make sure your comrade knows about this, and leave a note somewhere obvious.

You may want to come up with a media plan for what to do if you are injured, arrested, or killed.
Do you want your situation politicized or not?
Some people may want their lives to stay more anonymous, and others may trust their comrades to turn their misfortune into propaganda to advance the movement's goals.

\begin{figure}[htb]
\centering
\caption[Contingency Directives]{Contingency Directives\supercite{snailsnail}\textsuperscript{,}\footnotemark[3]}
\includesvg[height=6cm, keepaspectratio]{hong-kong-medic-helmet}
\end{figure}

\footnotetext[3]{
On October 1st, 2019, a photo surfaced of a medic wearing this helmet.\supercite{hong-kong-medic-helmet}
While this is not a legally recognizable DNR (Do Not Resuscitate), it shows the fear and desperation medics face during unrest and repression.
}

\subsection{Summary}

You should expect to organize in a non-hierarchical, autonomous way with formal collectives or loose affiliations.
Be wary of trying to apply old organizational patterns to your activism.
Avoid giving orders, and do not feel obligated to obey any you receive.
Work with a buddy for your own and your patients' safety.
Make preparations in case you get arrested or injured.
