\section{Police and the State}

\epigraph{
Anarchists know that a long period of education must precede any great fundamental change in society, hence they do not believe in vote-begging, nor political campaigns, but rather in the development of self-thinking individuals.
}{Lucy Parsons, \textit{The Principles of Anarchism}}

\noindent
Police brutality is a public health crisis; they beat civilians at will, they torture those they arrest, and they use crushing violence against protesters.
Moreover, police do not protect civilians; they protect the interests of the State and capital.
While it may be possible to de-radicalize individual police officers, as a unit, they are the primary opposition to protesters and those who fight for civil rights and liberation.
This has been made exceptionally clear in recent years where the Blue Lives Matter ideology, a reactionary mockery of the Black Lives Matter movement, has spread to police forces around the world, often accompanied by other symbology for police supremacy.

Depending on the privileges you have and how you are perceived by society, in particular your skin color and gender, you may have never experienced police harassment.
You may have never even had an encounter with the police.
If you have, it may have been simple or easily diffused.
Maybe your last run in was 15 years ago, but police have changed a great deal since then.

The way police treat protesters, even supposedly neutral persons, those protected by the Geneva Conventions like medics, can be a world of difference from how they act day to day.
Liberal democracy claims the police are a necessary tool to keep crime at bay, but in reality this means oppressing minorities and stopping dissent.
Your past experiences with the police may not accurately predict how they will interact with you when you take to the streets.

\subsection{Hear Nothing, See Nothing, Say Nothing}

Aside from the standard armored riot cops, or even the high visibility walls of cops with batons, are police intelligence officers.
Some may be plainclothes and mill about.
Some may wear specially colored vests with titles like ``Police Liason Officer,'' and regardless of what their claimed role is, they are there to learn about a protest's goals and key individuals.
This can be used to build cases against people or preemptively constrain a protest.
Do not talk to friendly cops, and better yet, do not talk about specifics with anyone unless you fully trust them and can avoid being overheard.

As mentioned when discussing patient confidentiality, you should avoid asking for names or other information that can be used to identify individuals.
Further, you should avoid asking questions that lead to answers where a patient can admit to a breaking laws.
This may be overheard, or you may be interrogated later.
You can't say what you don't know, so carefully phrase questions and cut off patients if they start spilling incriminating details.
For example, if a patient is holding their head, don't say ``What happened?'' because what happened is irrelevant.
The question ``Did you get hit in the head?'' is preferred because it tells you the mechanism of injury without revealing if they were involved in fighting the police.
Better yet, preface questions with phrases like ``without going in to detail'' or ``without telling me exactly how.''

Much of your time spent as a medic will be searching for people to help and finding places to be where you expect help will be needed.
You may see many things, and people around you may be engaged in illegal activity, but that is of no concern to you.
Don't watch.
Don't film.
If there's no incoming cops, walk away so that if the act is filmed, you won't be in the shot and called as a witness.

% TODO social media tips?

Generally, you should never speak to the police.
If police speak to you, find out if you are being detained or are required to stay.
If not, leave immediately.
If you are detained, do not speak until you have legal counsel.
Nothing you say will benefit you and you may get yourself into greater legal trouble.
The key phrases and exact steps you need to memorize to legally get yourself away from the police vary by region, so you will need to learn these on your own.

\subsection{Professional Courtesy}

While the safest advice is to never speak to the police, under any circumstances, without a lawyer present, often while on the streets, you may need to cross police lines or get access to injured persons.
Acting with the sort of professionalism you use in your hospital or praxis can help you talk your way in to being able to assist.
When doing this, you must be exceptionally careful to not say anything incriminating about yourself or others and to not reveal information about actions.
This is something you should practice with your buddy before deploying it on the streets.

If you have a uniform that is sufficiently professional, talking your way across police lines is significantly easier.
Attire is discussed more in \fullref{sec:tactics}.
In some regions, physicians have special identification cards that can be used to allow them to provide treatment during police operations.
Carrying identification with your legal name presents its own risks, as does showing it to police who have been known to collect photos of protesters and their IDs on their personal phones.
Talk to local medics and radicals, and investigate the degree to which police in your region violate civil liberties and people's privacy before attempting this.
You may consider it worthwhile if it helps you reach protesters more easily.

While this tactic may work, it may also raise suspicion among protesters about how and why you were able to cross police lines.
Is it because you're secretly a cop?
This can cause them to distrust you and even harass you later in the day or at another action.

\subsection{Summary}

The police are not your friends, and they actively oppose everything social movements stand for.
Do not talk to them without a lawyer present.
If you must in order to cross their lines or reach patients, be very cautious about what you say.
You can't reveal what you don't know, so intentionally being ignorant of crimes or patients' identities can protect both you and them.
