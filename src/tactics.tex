\section{Tactics}
\label{sec:tactics}

\epigraph{
As for politics, I’m an anarchist.
I hate governments and rules and fetters.
Can’t stand caged animals.
People must be free.
}{Charlie Chaplin, \textit{Charlie Chaplin: Interviews}}

\noindent
Medics are more akin to lifeguards than they are paramedics.
They are part of the crowd, and they have to actively seek out situations where violence may occur so they can quickly render care.
Tactics vary greatly with geography and time.
The best teachers are experienced medics in your region, and experience itself.

\subsection{Unfriendly Medics}

During popular uprisings, such as the Yellow Vests, there may be medics with right-wing views.
Not all medics have a liberatory outlook.
Be aware of this as you look for groups to work with or while on the street.
Right-wing medics are exceptionally rare, even at right-wing events, so this is mostly just a word of warning against blindly trusting all medics.

Further, not all medics who claim to have left-wing views are allies.
Snitches and infiltrators exist, and yes, they even take on the role of medic to camouflage themselves.
A notable example is the case of ``Anna the medic.''
Starting in 2004, she worked as an infiltrator leading to the 2006 arrest of green anarchist Eric McDavid and two others.\supercite{anna-medic, anna-medic-take-part}
Anna used her position as a street medic as an in to groups, and when called upon utterly failed to provide aid leading to at least one hospitalization.

\subsection{Attire}

How you select attire and equipment will depend on what sort of tactics your opposition employs, and to what extent medics are protected by the laws of liberal democracy.
In some regions, most notably the US, medics are explicitly targeted for violence and arrest.
In others, they are free to move relatively unimpeded.

\begin{figure}[htbp]
\caption{Medic Attire\supercite{snailsnail}}
\label{fig:medic_attire}
\centering
\includesvg[height=5.5cm, keepaspectratio]{medic-attire}
\end{figure}

In Figure~\ref{fig:medic_attire}, the three broad classes are medic are pictured.
For the purposes of discussion, they are named from left to right: uniformed medic, classic medic, and black medic.
Generally speaking, more professional uniforms and more ostensibly neutral behavior can mean less restriction of movement and more options for treatment, if the police act with restraint toward medics.
If this restraint does not exist, the uniform does nothing.

Uniformed medics are the rarest as they require very high levels of legal protection or at least a norm of medics being treated as neutral.
Their attire is close to the official EMS uniforms of their region.
They generally must act with neutrality to do their work and refrain from overt displays of politics.

Classic medics are fairly well marked and are quite common, especially during mass unrest.
They are marked clearly enough to be considered medics by bystanders, but generally their main protection from police violence and arrest is the police not perceiving them to be a threat.
Their role is medic, but they are active protesters, though not the main aggressor at an action.

Black medics may be only minimally marked and are generally dressed in ways that obscure their identity to help them avoid repression.
They often move as part of the Black Bloc, and there is no self-imposed restriction on actions.
This class has the disadvantage that their minimal markings make it hard for others to find them, so they need to be more alert and actively seek patients.

None of these classes is ``better'' or ``worse.''
In some regions, some classes are more common because they are more effective.
Talk to locals about what regional norms are for medics, but in all likelihood the classic medic attire and disposition is where you will start.

\subsection{At Actions}

For actions, coming up with a plan of how you think the day will play out lets you and your team make informed decisions on how to dress, what to bring, and where to position yourself.
Read up on the event and look at social media posts if they exist.
Print out a map of the expected areas where the actions will take place, and give a copy to everyone on the team.

It is exceedingly difficult to make statements about how any single medic or collective should act at an action.
Many of the factors one must consider are region specific.
However, the following three things are a reasonable baseline:

\begin{enumerate}
    \item Be safe, be alert.
    \item Move towards where you expect violence.
    \item Don't let a surging crowd pull you away from your duties.
\end{enumerate}

Other general advice is:

\begin{itemize}
    \item Be present at the starting and ending points of the action.
    \item Be near to blockades.
    \item If you are uniformed or obvious, be wary of drawing attention to people carrying out covert activities.
    \item If there are obvious radicals, be near them as they are often targeted for violence.
    \item When people break through a police line, hold back and look for casualties.
    \item When police charge, avoid over-retreating and leaving casualties behind.
    \item If a patient is being attended by a qualified provider and they do not need your help, don't stay. Go find another patient.
    \item Walk, don't run. Running medics spooks people.
    \item Hold the straps of your buddy's bag when moving through a crowd to avoid getting separated.
\end{itemize}

\subsection{Summary}

Experience is the best teacher, and there is no substitute for attending actions if you want to learn tactics.
Be alert, stick with your team, and actively seek out tension so you can provide aid.
With time, you will develop a sense for this and it will become natural.
