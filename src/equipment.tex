\section{Equipment}
\label{sec:equipment}

\epigraph{
The fulfillment of the revolutionary project is ultimately an inescapable ethical obligation that each of us have to the dead and the exploited.
}{Idris Robinson, \textit{How It Might Should Be Done}}

\noindent
Your equipment will be limited compared to what you have in your clinic or ambulance.
You will be limited to what you can carry on your back, and much of that volume and weight will be for the things you need to survive an action such as food, water, and personal protective equipment.

The three fundamental questions to ask yourself when selecting equipment are:

\begin{enumerate}
    \item What do I actually know how to use?
    \item What am I realistically going to need?
    \item What amount of weight can I actually carry?
\end{enumerate}

If you can't run, you can't stay with an action.
If you're slow, you may get arrested or attacked.
While you're new, less equipment and a lighter load is suggested.

Professional healthcare workers often feel unprepared when they don't have the full array of equipment they're used to, but you will need to learn to deal with this.
In particular, physicians often want for their intubation kit complete with laryngoscope.
Such equipment is large, heavy, expensive, and rarely used.
Consider cheaper and lighter alternatives even if they provide inferior care.

\subsection{Clothing}

In \fullref{sec:tactics}, attire style and appearance is discussed a bit more as it can change how police interact with you.
Generally, your clothing should be chosen so you can do physical activities in it.\footnotemark[1]
You should be able to run and move freely in it.
Avoid obvious or unique logos as these can be used by police or fascists to identify you.

\footnotetext[1]{
For example, very tight jeans or a dress might not allow you freedom of movement and can get caught on things when climbing.
Of course, you know yourself best, so pick what you find comfortable being active in, but keep in mind that you might have to run to safety in it.
Select accordingly.
}

Dress in layers so you can add or remove clothing as necessary.
Riot control agents bind to cotton, wool, and other natural textiles, but synthetic material such as nylon can melt to your skin as a result of coming in contact with explosives, other pyrotechnics, or burning tear gas cannisters.
Layering allows you to wear both a heat resistant layer and a riot control agent resistant layer.

Close-toed shoes are mandatory.
Hiking boots are recommended because of their grip and protection for your ankles.
Large punk boots are less recommended as they are harder to run in.
Running shoes are also a good choice, though they offer less ankle and puncture protection.

A utility vest is a good addition to your attire as its many pockets make it easy to store frequently used items like gloves, gauze pads, and hand sanitizer.
Pants or shorts with large pockets can also fulfill this role.

Something like a gaiter or keffiyeh is a good choice to protect your neck from riot control agents.
They can also be used to conceal your identity if necessary.

\subsection{Personal Protective Equipment}

You need to protect yourself to be an effective medic.
If a baton round fractures your skull or you are blinded by tear gas, you cannot do your job.
You want to protect your head, eyes, ears, lungs, and everything else (in that order), and you want to do so in a way that isn't so conspicuous as to make you stand out in the crowd and draw police attention.
The rule of thumb is if others have PPE, you should too, and if they don't, neither should you.
Also, it may not even be legal to wear any PPE as it can be classified as a ``passive weapon'' as it makes you more resistant to police violence.
Look up local laws and norms among protesters.
A complete discussion of all the personal protective equipment (PPE) you might want and their trade-offs would be as long as this entire guide, so the following is a reasonable selection for getting started on a budget.\footnotemark[2]

\footnotetext[2]{
CrimethInc's \textit{A Demonstrators Guide to X} series\supercite{crimethinc-goggles, crimethinc-helmets, crimethinc-munitions, crimethinc-armor} and the Indigenous Anarchist Federation's \textit{Skills for Revolutionary Survival} series\supercite{iaf-ppe, iaf-ballistic} are great guides on PPE.
}

For your head, ideally you would get a ballistic helmet.
If this is not possible, an EMS-style rescue helmet, a bike helmet (specifically BMX), or climbing helmet are fine choices.
Avoid helmets without a chinstrap.
Find one that fits snugly on your head and can accommodate a respirator and goggles.
If you cannot wear a helmet, consider getting bump cap (baseball hat with a hard plastic lining).

For your eyes, you can use a full-face respirator, or pair it with a half-mask respirator and googles.
A common combination is to have have two pairs of goggles.
Swim googles as the first pair protect against riot control agents.
Ballistic goggles that have the lens laminated with a clear, durable tape seems to be able to stop many types of munitions.
A simpler options is to use safety glasses or goggles, though this only is effective against shrapnel and glass, not munitions.
All eyewear should be clear as even light tint significantly impairs vision at night.

\begin{figure}[htb]
\caption{Types of Respirators}
\centering
\begin{subfigure}[b]{0.3 \textwidth}
    \centering
	\includesvg[width=\textwidth, keepaspectratio]{full-face-respirator}
	\caption{Full-Face}
\end{subfigure}
\begin{subfigure}[b]{0.3 \textwidth}
    \centering
	\includesvg[width=\textwidth, keepaspectratio]{half-mask-respirator}
	\caption{Half-Mask}
\end{subfigure}
\begin{subfigure}[b]{0.3 \textwidth}
    \centering
	\includesvg[width=\textwidth, keepaspectratio]{filtering-half-mask}
	\caption{Filtering Half-Mask}
\end{subfigure}
\end{figure}

For masks, a P100 or FFP3 filtering half-mask is the simplest and cheapest.
A half-mask can be paired with better ballistic eye protection, and a full face can be used as a combination protection for eyes and your lungs.
Half-mask respirators are useful as you will not have to replace their visor if they're damaged.

\begin{figure}[htb]
\caption{Gloves\supercite{snailsnail}}
\centering
\begin{subfigure}[b]{0.29\textwidth}
    \centering
    \includesvg[width=\textwidth, height=4cm, keepaspectratio]{rescue-gloves}
    \caption{Rescue}
\end{subfigure}
\begin{subfigure}[b]{0.35\textwidth}
    \centering
    \includesvg[width=\textwidth, height=4cm, keepaspectratio]{work-gloves}
    \caption{Work}
\end{subfigure}
\begin{subfigure}[b]{0.26\textwidth}
    \centering
    \includesvg[width=\textwidth, height=4cm, keepaspectratio]{assault-gloves}
    \caption{Assault}
\end{subfigure}
\end{figure}

For your ears, use earplugs with a Noise Reduction Rating (NRR) of -30dB.
For your knees, plastic skate pads or soft handball pads are useful if you trip of have to kneel over a patient.
For your hands, rescue gloves, assault gloves, or work gloves suffice.

\subsection{Medical Equipment}

For your bag, it is recommended to use an EMS style bag that opens like a clamshell.
You can find cheap, small EMS bags; you do not need to get a professional bag.
If your bag does not come with modular pouches, you should acquire those as they significantly help with organizing your equipment.

\begin{figure}[htbp]
\centering
\caption{EMS Backpack\supercite{baedr}}
\includesvg[width=\textwidth, height=6cm, keepaspectratio]{ems-backpack-open}
\end{figure}

The items in the following tables are suggested for a starting medical kit that covers both the most common injuries and the sort of severe injuries you are qualified to handle.
Optional items have their quantity written in \textit{italics}.
In addition, pack the PPE you need plus food and drinking water.
Keep any medications you might need if arrested on your person (not in your bag).

\begin{table}[htbp]
\footnotesize
\caption{Packing List --- First Aid}
\centering
\begin{tabularx}{\linewidth}{r|X}
    \textbf{Qty.} & \textbf{Item} \\
    \hline
    1          & Trauma shears \\
    3-5        & Emergency blanket \\
    2-4        & Instant cold compress \\
    1          & Vomit bag \\
    10         & Dextrose \& salt drink mix packet \\
    5          & Single-use saline vials \\
    1          & \SI{500}{\mL} pneumatic eyewash bottle \\
    1          & Tweezers \\
    1          & Splint \\
    1          & Cervical collar \\
    \textit{1} & Can refrigerant spray \\
    \textit{5} & Paracetamol tablets \\
    \textit{5} & Ibuprofen tablets \\
    \textit{5} & Aspirin tablets \\
    \textit{5} & Anti-histamine tablets \\
    \textit{1} & Salbutamol inhaler w/ spacer \\
    \textit{1} & Epinephrine autoinjector \\
    \textit{1} & Narcan spray bottle \\
\end{tabularx}
\end{table}

\begin{table}[htbp]
\footnotesize
\caption{Packing List --- Wound Care}
\centering
\begin{tabularx}{\linewidth}{r|X}
    \textbf{Qty.} & \textbf{Item} \\
    \hline
    20                     & Individual package gauze (10$\times$\SI{10}{\cm}) \\
    10                     & Gauze roll \SI{10}{\cm} \\
    2                      & Non-adhesive dressing (10$\times$\SI{10}{\cm}) \\
    1-2                    & Roll \SI{2}{\cm} medical tape \\
    1                      & Roll \SI{5}{\cm} medical tape \\
    \textit{2}             & Elastic net dressing \\
    3                      & Self-adhering bandage (\SI{2}{\cm}) \\
    2                      & Self-adhering bandage (\SI{5}{\cm}) \\
    2                      & Elastic bandage \\
    \textit{1-2}           & Combat dressing \\
    1                      & Chest seal (pair) \\
    1                      & Package hemostatic gauze \\
    5                      & Package wound closure strips \\
    20                     & Adhesive bandages (assorted size) \\
    1-2                    & Triangle bandage \\
    1                      & Tourniquet \\
    \textit{1}             & Burn dressing (10$\times$\SI{10}{\cm}) or burn gel (\SI{50}{\mL}) \\
    \SI{100}{\mL}          & Antiseptic spray \\
    \SI{50}{\mL}           & Antibacterial creme \\
    \textit{\SI{400}{\mL}} & Antiseptic irrigation solution \\
     2                     & Irrigation syringe (\SI{30}{\mL}) \\
\end{tabularx}
\end{table}

\begin{table}[htbp]
\footnotesize
\caption{Packing List --- Infection Control}
\centering
\begin{tabularx}{\linewidth}{r|X}
    \textbf{Qty.} & \textbf{Item} \\
    \hline
    10-15         & Pair Examination gloves \\
    \SI{100}{\mL} & Hand sanitizer \\
    2-4           & Surgical mask \\
\end{tabularx}
\end{table}

\begin{table}[htbp]
\footnotesize
\caption{Packing List --- Basic Life Support}
\centering
\begin{tabularx}{\linewidth}{r|X}
    \textbf{Qty.} & \textbf{Item} \\
    \hline
    1          & Keychain CPR mask or pocket CPR mask or BVM \\
               % TODO list sizes. ORA 70-110mm, NPA ???
    \textit{5} & Oropharyngeal and/or nasopharyngeal airway (sized large child to adult) \\
    \textit{2} & Disposable razor \\
    1          & Tension pneumothroax access kit \\
\end{tabularx}
\end{table}

\begin{table}[htbp]
\footnotesize
\caption{Packing List --- Diagnostic Equipment}
\centering
\begin{tabularx}{\linewidth}{r|X}
    \textbf{Qty.} & \textbf{Item} \\
    \hline
    1          & Pulse oximeter \\
    1          & Penlight \\
    \textit{1} & Stethoscope \\
    \textit{1} & Blood pressure cuff \\
    \textit{1} & Blood glucose meter (with lancets and test strips) \\
    \textit{1} & Thermometer (with disposable covers) \\
\end{tabularx}
\end{table}

\begin{table}[htbp]
\footnotesize
\caption{Packing List --- Misc.}
\centering
\begin{tabularx}{\linewidth}{r|X}
    \textbf{Qty.} & \textbf{Item} \\
    \hline
    2  & Package tissues \\
    5  & Tampon (assorted sizes) \\
    2  & Plastic trash bag \\
    10 & Pair earplugs \\
    10 & Safety pins \\
\end{tabularx}
\end{table}

% To attempt to force the tables into a nice position
\clearpage
