\section{Introduction}

\epigraph{
You cannot buy the revolution.
You cannot make the revolution.
You can only be the revolution.
It is in your spirit, or it is nowhere.
}{Ursula K. Le Guin, \textit{The Dispossessed}}

\noindent
As a medical professional\footnotemark[1], you have a wealth of skills and experiences that make you valuable to the social movements that try to bring about a more liberated and egalitarian world.
Where there is struggle against the State, capital, and fascists, there are physical confrontations, and where there are physical confrontations there are injuries.
Members of these movements provide medical care to tend to those who have been injured.
They go by many names: street medics, action medical, riot medics, or often simply ``the medics.''
They are one of the many forms of self-organized mutual aid that contribute to a movement's success.

\footnotetext[1]{
If you're not a medical professional (and you skipped the warning in the disclaimer), you likely want to read the full \textit{Riot Medicine} text.
It's much longer and assumes you have no prior medical knowledge.
}

\begin{figure}[htb]
\centering
\caption[Medics in France Hiding From Riot Cops]{French Medics Hiding from Cops\supercite{snailsnail}\textsuperscript{,}\footnotemark[2]}
\includesvg[width=0.7\textwidth, keepaspectratio]{french-medics-hiding}
\end{figure}

\footnotetext[2]{
On January 11th, 2019, this photo\supercite{pedro-fonseca} was taken in Paris showing medics and journalists hiding around the corner from riot cops.
}

When shifting away from a classic medical setting, you may find yourself stationed at a refugee camp, helping the unhoused, or offering consultations at a community health clinic.
There are many ways to merge medicine with the fight for a better world.
This guide is, however, more narrow in scope.
It does not cover how to run a clinic or anything related to long-term care within your community.
From its name, \textit{Riot Medicine}, the focus is on medicine in the contexts of protest and insurrection.
You, of course, can both change the bandages of the residents of your local tent city and also show up to riots to treat concussions and other trauma.
They are not mutually exclusive, but if you are not planning on attending actions, this pamphlet may not be for you.

What this pamphlet will do is help you bridge the gap from working in a clinical setting or classic EMS to working in the sort of non-hierarchical, self-organized, and more chaotic contexts of riot medicine.
Perhaps unrest has broken out in your city and you want to join those who have taken to the streets to fight racism, fascism, and other forms of oppression.
Perhaps you have no experience working with radicals, and perhaps you've never been to a protest that got rowdy.
That's ok.
Everyone starts somewhere, and this pamphlet will help you understand what to expect in while organizing with others and while on the street.

This pamphlet is written from an autonomous, anarchist perspective.
However, the information and tactics described within will be useful to all participants in the struggle for liberation.
State imposed laws and regulations are a reality, and where it is relevant, it is noted where your work may intersect with the legal system to highlight what legal risks there may be.
This pamphlet was written in 2021, so as you are reading this, be wary that medical best practices, legal considerations, and all other information may have become out of date.

By reading this, you will be better prepared to take your medical knowledge to the streets and to care for and heal those on the front lines.
You may be only one person, but you contributions, however small, help change the world for the better.
