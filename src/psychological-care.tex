\section{Psychological Care}

\epigraph{
If trauma results from perceiving a threat to one’s existence or the existence of  another living being whom one cares about, then a necessary step in healing from trauma is securing a sense of safety from existential threats to oneself or  others.
}{Jane Adams Collective, \textit{Mutual Aid, Trauma, \& Resiliency}}

\noindent
Many injuries and illnesses you see at actions will be relatively minor, but the long-lasting effects of psychological trauma from police brutality or surprise fascist attacks can be far more damaging.
This section covers the framework known as Psychological First Aid (PFA).
When effectively applied, PFA can reduce the chances and severity of PTSD.

If you've worked in disaster relief, you may have encountered another framework known as Psychological Debriefing (PD).
PD is not effective in reducing PTSD and may cause harm, and thus it is not recommended.\supercite{effective-treatment-ptsd, psych-first-aid}

As a note, I have adapted PFA's steps and how they are applied as they were intended for use by centralized organizations like NGOs and the military for dealing with civilians following MCIs such as natural disasters or terrorist attacks.
In riot contexts, things go a bit differently, so even if you're already familiar with PFA, I would recommend you read this section regardless.

\subsection{PFA Basics}

Psychological first aid is a framework for providing supportive, emotional care to individuals who have experienced traumatic events.
It is not some exact set of steps, and you should adapt it to the cultural and social norms of your region.
Like how medical first aid is the initial step into managing a wound, PFA is the initial step in the process of recovery from trauma.
The major goals of PFA include:

\begin{itemize}
    \item Helping individuals feel safe
    \item Avoiding retraumatizing or additionally traumatizing individuals
    \item Connecting individuals to other individuals and groups who can provide further aid
    \item Restoring a sense of autonomy
\end{itemize}

\subsection{PFA Steps}

PFA uses some combination of the following steps.

\triplesubsection{Ensure your own safety}
Like with other first aid, you need to ensure your own safety.
If police are still actively terrorizing individuals, you may not be able to intervene and begin care.
If fascists have made an incursion into your action and there is street fighting, you may not be able to help until the violence has died down.

\triplesubsection{Identify who needs care}
Identify who needs care and quickly triage them.
Individuals with moderate and severe physical injuries should take priority over individuals who have been traumatized.
The basics of PFA is kindness and calmness which is something most people can provide, but you as a medic may be one of a small number of people who can offer medical care.
Individuals who have been on the receiving end of violence may not always be the ones who need care as they may have expected said violence and used proactive coping measures to prepare for it.
Look for individuals who seem frozen, dazed, or panicked.
They are the ones more likely to need PFA.

\triplesubsection{Introduce yourself and obtain consent}
Like with first aid, identify yourself and obtain consent to treat the individual.
State your qualifications, what you are able to offer, and ask if you can assist them.

\triplesubsection{Remove the patient from the source of trauma}
If the patient is still in a chaotic environment or near where a traumatic incident took place, you may not be able to care for them or help calm them.
Consider moving them somewhere that is calmer and has a greater sense of security than their current location.
This may not be possible, or you may need to stay close to the area where the traumatic event occurred so that others may be able to find you for medical care.

\triplesubsection{Separate the calm from the panicked}
If you have multiple patients, you may need to separate the calm-but-traumatized from the panicked patients.
The patients who are panicked and highly animated may erode the small amount of calmness in other patients.

\triplesubsection{Keep nosy busybodies away from patients}
Bystanders may be curious about what happened and ask invasive questions to the patients.
This may further traumatize them as it may force them to speak about the traumatic event, and it can interfere with their ability to feel calm and safe.
Journalists may try to take photos or ask questions to get a scoop.
Keep busybodies and journalists away from patients.

\triplesubsection{Help the patient feel safe, comfortable, and calm}
Help the patient feel safe.
Moving them to a safe location is a good first step.
Telling them that you are here to help and that others are there for support can be helpful.
Offer them something to drink or a snack.
If they are crying, offer them tissues.
While quite simple, this is a powerful gesture that symbolizes both care and a return to normalcy.
Speak to them in a calm and reassuring voice, both in tone and content.

\quadruplesubsection{Keep the focus on the patient}
Ask the patient about how they feel, and avoid making your interaction about how you feel.
Keep your judgement about the event and your appraisal of their response out your treatment.
It does not matter if you think their reaction to the traumatic event is appropriate or not.
Don't tell them they're overreacting.
All feelings are valid.
You are there to provide care and support regardless of your interpretation of the event.

\quadruplesubsection{Do not lie or misrepresent the situation}
While you attempt to calm and reassure the patient, do not lie about resources available or the current situation.
If there are unknowns, you may state so, and you may choose to omit some details if they are not relevant or helpful.
However, do not make statements like ``everyone is fine'' or ``nothing bad is going to happen to you now'' unless you know with certainty that such statements are true.

\triplesubsection{Be mindful of how you communicate}
Communication isn't just words alone but tone, facial expressions, and posture.
Try to see yourself from the patient's eyes considering your relation to them both culturally as well as through your relative privileges.
Maintain a pleasant disposition.
Face your body towards them, and keep your focus on them.
If they are sitting, either sit or squat so you do not loom above them.
When they talk, nod your head so they can see that you are actively listening.

\triplesubsection{Consider offering physical contact}
Some patients may find physical contact to be helpful and calming.
This may mean a hand on their shoulder, holding their hands, or a hug.
Ask for content before touching a patient, and be explicit about what kind of physical contact you are going to offer.
For example, clearly say, ``would you like a hug?''

\triplesubsection{Prompt the patient to speak}
Ask the patient if they would like to speak about what happened.
Do not force them to discuss anything as this may retraumatize them.
Allow for long pauses as they collect their thoughts.
Repeatedly asking them questions can add to the stress of the situation and interrupt their train of thought.
One exception to this is if there is still an active threat and they were one of a small number of witnesses.
If there is still danger (like an armed assailant), you may need to get them to answer basic details to help protect others.

\triplesubsection{Use grounding techniques}
The patient may be panicked or disassociated from the current situation.
Encourage them to breathe slowly.
Help them identify 5 things they are feeling (physically) and 5 things they can see or hear that are calming.
Ask them what they see in the clouds, what the weather is like, or if they can hear any birds.
Have them put their feet firmly on the floor or their hands in the lap, and ask them to describe the feeling.

\triplesubsection{Enable self sufficiency}
Once you have attended to the patient's immediate needs like safety and minor material comforts, ask them what they need.
Do they need to get home?
Do they have friends at the action who can help them?
Are they worried about someone else at the action, and can you help them find this person?
Do they want to be connected with more qualified providers of psychological care?
Facilitate their own autonomy.

\quadruplesubsection{Create a plan}
Help the patient come up with a short-term plan they can immediately enact.
For example, make a plan to get them home.
Discuss this with them, and then act on it.

\triplesubsection{Connect them to support}
Help connect the patient to support in their life such as friends, family, or other organizations that work with activists.
Ask them what they have done to cope in the past, and suggest they try using these measures again.
If you know of support networks for activists or traumatized individuals, provide them with business cards or a means of contacting this additional care.
Even something as simple as a left-leaning community kitchen where they can meet others may have some benefit.

\triplesubsection{Suggest and encourage positive coping}
Your care may be brief, but the patient may need ongoing care.
Some of this care can be done on their own, and you can encourage healthy coping strategies such as:

\begin{itemize}
    \item Getting enough sleep
    \item Remembering to eat and drink enough water
    \item Staying connected with and talking to friends, family, and comrades
    \item Finding someone with whom they can talk about their trauma
    \item Spending time with their pets
    \item Continuing to engage in fun and relaxing activities like game night, reading, or seeing live music
    \item Exercising and spending time outdoors, even if it's as simple as walking around their neighborhood
\end{itemize}

\triplesubsection{Discharging your patient}
You do not want your patient to feel abandoned.
You may have to care for others, and if you need to leave your patient temporarily, tell them so, and tell them you will be back.
If you need to end care because they are leaving or you need to leave, ensure they have been connected to someone or another group that can continue to care for them.
This connection should not be vague and abstract, but concrete.
Introduce them to someone, and ensure that the next provider is aware that you are transferring care.
Of course, no such provider may be available, and you do not have unlimited time to spend with each patient.
You may not be able to give a proper hand-off, and you may have to attend to your own needs.
Do what you can to make the termination of care not feel abrupt.

\subsection{Summary}

As it has been repeated, one of the ways a medic can help heal patients is by being a beacon of calm amidst chaos and panic.
This calm can be used for organizing other medics and guiding treatment.
Furthermore, it is a critical component of psychological first aid.
Remember that calmness and composure do not mean that a medic needs to be stoic and lacking in outward compassion.
PFA involves making the patient feel safe and calm, attending to their basic needs, avoiding retraumatizing them, and connecting them to additional means of care.
Help the patient come up with a short-term plan they can act on to help them restore their autonomy.
