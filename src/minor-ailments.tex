\section{Minor Ailments}

\epigraph{
The government is not giving anything, and even with the protests pressuring them, they have yet to come up with anything in terms of any kind of program to prevent further atrocities against Black people.
}{Lorenzo Kom'boa Ervin\supercite{lorenzo-interview}}

\noindent
Many of the injuries and illness you encounter are trivial to care for and are covered by basic first aid.
Some of the less trivial injuries and illness are covered here.

\subsection{Fractures and Dislocations}

Treatment for fractures is to splint the joints above and below the fracture.
Treatment for dislocations is to splint the bones above and below the joint.

\triplesubsection{Fingers}
Fingers can be splinted individually using commercial or improvised splints.
Another technique is buddy taping where the injured finger is taped to an adjacent finger for support (\autoref{fig:buddy_tape_finger}).
When buddy taping, place gauze between the fingers to prevent chaffing and maceration.

\triplesubsection{Toes}
Toes are buddy taped in much the same way fingers are (\autoref{fig:buddy_tape_toe}).
Additionally, user a long strip to tape the toes to the top of the foot to provide support.
This is especially important if the pinky toe is injured.
Because the toes are small, you may need to cut your strips of tape in half lengthwise.

\begin{figure}[htbp]
\caption{Buddy Taping\supercite{baedr}}
\centering
\begin{subfigure}[b]{4cm}
    \centering
	\includesvg[width=\textwidth, height=4cm, keepaspectratio]{buddy-taping-finger}
	\caption{Finger}
    \label{fig:buddy_tape_finger}
\end{subfigure}
\begin{subfigure}[b]{4cm}
    \centering
	\includesvg[width=\textwidth, height=4cm, keepaspectratio]{buddy-taping-toe}
    \caption{Toe}
    \label{fig:buddy_tape_toe}
\end{subfigure}
\end{figure}

\triplesubsection{Hand}
Injuries to the hand that are not limited to the fingers require splinting the wrist.
Splint the hand in the position of function.
This is slightly open as when the hand hangs freely when at one's side.

\triplesubsection{Arm and shoulder}
Slinging and swathing is used to immobilize the arm and shoulder (\autoref{fig:sling_and_swathe}).
This is particularly useful for clavicle fractures.

\quadruplesubsection{Sling}
A sling can be made using a triangle bandage.
Hold the triangle bandage up to their chest so that the longest side is vertical and opposite the side with the fracture.
Have the patient place their hand on their breast.
Pull the top corner around the back of their neck and tie it to the bottom corner.
The third corner should be approximately at their elbow with enough extra  material to make a flap.
This flap will cup their elbow and prevent their arm from sliding out of the sling.
Fold this flap around their elbow and use safety pins to secure the flap.
Alternatively, use a long strip of duct tape to secure the flap.

\quadruplesubsection{Swathe}
Use a second triangle bandage, strips of cloth, or webbing to secure the arm to the patient's torso.
Injuries to the elbow may make it impossible to sling and swathe, so you may need to improvise immobilization.

\begin{figure}[htbp]
\centering
\caption{Sling and Swathe\supercite{baedr}}
\label{fig:sling_and_swathe}
\includesvg[width=\textwidth, height=8cm, keepaspectratio]{sling-and-swathe}
\end{figure}

\subsection{Strains and Sprains}

Strains, sprains, and tendinitis are damage to muscle, ligaments, and tendons from trauma or overuse.
Soft tissue may become inflamed, stretched, or torn.
Field treatment for athletic injuries is to immobilize, facilitate healing, avoid further injury, and manage the pain.
Patients should be sent home even for minor injuries as they can become dangerous if the situation escalates.

\triplesubsection{Consider wrapping and splinting}
If you cannot differentiate an athletic injury from a fracture or dislocation, assume the worst and immobilize it.
Sprains that lead to an unstable joint, strains that lead to significant loss of function, or injuries with severe pain likely require advanced medical care.

Body parts can be wrapped to provide stability during evacuation and to protect them from further injury.
Wrapping can also help with tendinitis.
For example, wrapping a wrist can help minimize forearm tendinitis.

\triplesubsection{Consider use of NSAIDs}
Use of NSAIDs can help reduce pain and swelling.

\triplesubsection{Use the PRICE method}
For the first 24 to 48 hours after injury, use the PRICE method to minimize pain and swelling.

\begin{table}[htbp]
\caption{PRICE Method}
\centering
\begin{tabular}[c]{c l}
    \hline
    \textbf{P} & Protection  \\
    \textbf{R} & Rest        \\
    \textbf{I} & Ice         \\
    \textbf{C} & Compression \\
    \textbf{E} & Elevation   \\
\end{tabular}
\end{table}

\quadruplesubsection{Protection}
The injured joint or muscle needs to be immobilized or padded to prevent from further injury.

\quadruplesubsection{Rest}
The patient needs to rest which means leaving the action.
For aftercare, a rule of thumb is that a patient should rest the injured body part until they can do simple activities without pain.

\quadruplesubsection{Ice}
Ice helps minimize swelling.
Ice can be applied for a maximum of 20 minutes per hour, allowing the injury to warm naturally between applications.
A towel or bandages should be placed between the ice and the skin.

\quadruplesubsection{Compression}
Use of an elastic bandage for compression helps reduce edema and provides some immobilization.

\quadruplesubsection{Elevation}
Elevating an injured limb above the heart helps reduce edema and associated pain.

\triplesubsection{Use the No HARM method}
For the first 72 hours, use the No HARM method to avoid further injury.

\begin{table}[htbp]
\caption{No HARM Method}
\centering
\begin{tabular}[c]{c l}
    \hline
    \textbf{H} & Heat      \\
    \textbf{A} & Alcohol   \\
    \textbf{R} & Re-injury \\
    \textbf{M} & Massage   \\
\end{tabular}
\end{table}

\quadruplesubsection{No heat}
Application of hot packs or submersion in a hot bath should be avoided.

\quadruplesubsection{No alcohol}
Consuming alcohol can increase blood flow leading to increased swelling.
More importantly, it can decrease sensitivity to the injury leading to aggravation of the injury.

\quadruplesubsection{No re-injury}
Related to resting, engaging in activities that could re-injure the body part should be avoided.

\quadruplesubsection{No massage}
Avoid massages as they may cause additional tissue damage.

\triplesubsection{Consider evacuation}
Resting and avoiding re-injury may mean that patients need to leave the action.
Their injuries may prevent them from running from danger or fighting to protect themself.

\triplesubsection{Recommend light exercise}
After the first 24 to 48 hours, light exercise helps promote healing.
Ideally, the patient should seek physiotherapy, but in the absence of that, a general rule is to do light exercises that promote strength in affected body part.
Recommend this to the patient.

\subsubsection*{Wrapping Techniques}

A wrap should be snug (but not tight), provide support, and somewhat immobilize the joint.
All wraps should be done with an elastic bandage or self-adhering bandage.

\triplesubsection{Wrapping an Ankle}
When wrapping an ankle (\autoref{fig:ankle_wrap}), the foot should be at a 90 degree angle to the leg.
Start the wrap on the bridge of the foot, then make figure 8's around the ankle.
Finish the wrap by wrapping around the Achilles tendon.

\begin{figure}[htbp]
\caption{Wrapping Joints\supercite{baedr}}
\centering
\begin{subfigure}[b]{5.3cm}
    \centering
	\includesvg[width=\textwidth, height=6cm, keepaspectratio]{ankle-wrap}
	\caption{Ankle Wrap}
    \label{fig:ankle_wrap}
\end{subfigure}
\begin{subfigure}[b]{5.3cm}
    \centering
	\includesvg[width=\textwidth, height=6cm, keepaspectratio]{wrist-wrap}
    \caption{Wrist Wrap}
    \label{fig:wrist_wrap}
\end{subfigure}
\begin{subfigure}[b]{0.3\textwidth}
    \centering
	\includesvg[width=\textwidth, height=6cm, keepaspectratio]{knee-wrap}
    \caption{Knee Wrap}
    \label{fig:knee_wrap}
\end{subfigure}
\end{figure}

\triplesubsection{Wrapping a Wrist}
When wrapping a wrist (\autoref{fig:wrist_wrap}), the hand should be open, the fingers spread, and the wrist in line with the forearm.
Start the wrap on the forearm, then make diagonal wraps up the hand.
One wrap should fully wrap the palm of the hand to serve as an anchor.
Make additional diagonal wraps back toward the wrist.
Finish by securing the wrap on the wrist.

\triplesubsection{Wrapping a Knee}
When wrapping a knee (\autoref{fig:knee_wrap}), all wraps should be as close to the joint as possible.
Because of the tapered shape of the quadriceps and calf muscles, wraps that are too far from the joint will slide back toward the joint and become loose.
Start the wrap above the knee making two circles to act as an anchor.
Make a diagonal wraps, crossing between the upper and lower leg on the back of the knee.

\subsection{Protester's Malaise}

A common illness you may see is something I've dubbed ``protester's malaise.''
It's a non-specific set of symptoms that result from dehydration, mild intoxication, and under-eating (hypoglycemia).
It's often exacerbated by heat, especially during the first hot protests of spring or summer.
This malaise tends to affect younger people, generally under 25, and is the result of simply not taking care of one's self.
Hours of marching or rioting without refueling leads to symptoms including: a general feeling of unwellness, lethargy, disorientation, nausea, and vomiting.

Treatment for this sort of generalize malaise is to move the person somewhere warm (on cold days) or cool (on hot days), give them a small amount of water, and dextrose tablets.
If you have a BG monitor, check their BG and diabetic status before giving them dextrose.
Have them drink slowly so they don't vomit.
Suggest they eat a sugary meal and drink more water after you leave, or suggest they just go home for the day.
