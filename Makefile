DEFAULT_GOAL := help
OPEN = $(word 1, $(wildcard /usr/bin/xdg-open /usr/bin/open))
TARGET = riot-medicine-bridge-guide
TARGET_IMPOSED = $(TARGET)_imposed
COMMIT_TEX = git-commit.tex
PDF_CMD = pdflatex -file-line-error -halt-on-error -shell-escape -interaction batchmode

.PHONY: help
help: ## Print the help message
	@awk 'BEGIN {FS = ":.*?## "} /^[0-9a-zA-Z_-]+:.*?## / {printf "\033[36m%s\033[0m : %s\n", $$1, $$2}' $(MAKEFILE_LIST) | \
		sort | \
		column -s ':' -t

.PHONY: clean
clean: ## Clean the workspace
	rm -rf svg-inkscape/ images/binary $(COMMIT_TEX)
	for x in acn acr alg aux bbl bcf blg dvi fdb_latexmk fls glg glo gls idx ilg ind ist log out pdf run.xml tmp toc; do \
		find . -path ./.git -prune -o -name "*.$$x" -type f -exec rm -rf {} \; || exit 1; done

.PHONY: lint
lint: ## Check the .tex files for errors
	@./helper.py lint

.PHONY: all
all: lint dot pdf check strip-metadata ## Run all step necessary to generate the final document

.PHONY: check
check: $(TARGET).pdf ## Check the LaTeX log for signs of errors
	@./helper.py check $(TARGET)

.PHONY: dot
dot: images/binary $(patsubst dot/%.dot,images/binary/%.png,$(wildcard dot/*.dot)) ## Convert all .dot files to .svg

images/binary:
	@mkdir -p images/binary

# This rule additionally strips the metadata
images/binary/%.png: dot/%.dot
	dot -Gdpi=300 -T png $< -o $@ && \
		exiftool -overwrite_original -all= $@

strip-metadata: $(TARGET).pdf check ## Strip metadata from objects
	pdftk $(TARGET).pdf dump_data | \
		sed -e 's/\(InfoValue:\)\s.*/\1\ /g' | \
		pdftk $(TARGET).pdf update_info - output $(TARGET).pdf.tmp && \
		mv $(TARGET).pdf.tmp $(TARGET).pdf
	
.PHONY: pdf
pdf: $(TARGET).pdf ## Create the PDF

.PHONY: pdf-imposed
pdf-imposed: $(TARGET_IMPOSED).pdf ## Create the imposed PDF

.PHONY: $(TARGET_IMPOSED).pdf
$(TARGET_IMPOSED).pdf:
	pdfbook2 -n -p a4paper -s $(TARGET).pdf && \
		mv $(TARGET)-book.pdf $(TARGET_IMPOSED).pdf

.PHONY: first-pass
first-pass: dot output-commit
	$(PDF_CMD) -draftmode $(TARGET).tex

$(TARGET).pdf: first-pass bib
	$(PDF_CMD) -draftmode $(TARGET).tex && \
		$(PDF_CMD) $(TARGET).tex

.PHONY: fast-pdf
fast-pdf: dot ## Quickly make the PDF without re-indexing
	$(PDF_CMD) $(TARGET).tex

.PHONY: bib
bib: first-pass
	biber --isbn-normalise --onlylog $(TARGET)

.PHONY: open
open: ## Open the PDF
	@if [ -f $(TARGET).pdf ]; then \
		xdg-open $(TARGET).pdf ; \
	else  \
		echo 'Error: PDF not found' ; \
		exit 1 ; \
	fi

.PHONY: word-count
word-count: ## Print the number of words in the document
	@if [ -f $(TARGET).pdf ]; then \
		pdftotext $(TARGET).pdf - 2> /dev/null | wc -w ; \
	else  \
		echo 'Error: PDF not found' ; \
		exit 1 ; \
	fi

.PHONY: index-spellcheck
index-spellcheck: ## Print the possibly misspelled words in the index
	./helper.py show-index | sed -e 's/\\((){}|\\)/ /g' | sort -u | aspell list | sort -u

.PHONY: output-commit
output-commit: ## Write the current git commit locally so that it can be included in the doc
	@git rev-parse --short HEAD | tr -d '[:space:]' > $(COMMIT_TEX) && \
		{ git diff HEAD --quiet || printf -- '-uncommited' >> $(COMMIT_TEX) ; }
