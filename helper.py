#!/usr/bin/env python3
import os
import re
import subprocess
import sys
from argparse import ArgumentParser
from os import path

ROOT_DIR = path.dirname(__file__)
IS_TTY = sys.stdout.isatty()

FOOTNOTE_MARK_RE = re.compile(r'\\footnotemark\[(\d*)\]')
FOOTNOTE_TEXT_RE = re.compile(r'\\footnotetext\[(\d*)\]')
INDEX_RE = re.compile(r'\\index\{[^\}]*\}', re.MULTILINE)
TOKENIZE_RE = re.compile(r'\s+')

LOG_CHECKS = {
    'There were undefined references': 'Undefined references',
    'LaTeX Warning: Reference `': 'Undefined references',
    'Label(s) may have changed': 'Incorrect labels',
    'LaTeX Warning: Citation `': 'Missing citation',
    'Package biblatex Warning: The following entry could not be found': 'Missing citation',
    "' multiply defined": 'Duplicate label',
    'No file ': 'Missing input file',
}

BLG_CHECKS = {
    'WARN - ': 'Misc. warning',
}


def colorize(msg: str, color: str, bold: bool = False) -> str:
    if not IS_TTY:
        return msg

    shell_colors = {
        'gray': '30',
        'red': '31',
        'green': '32',
        'yellow': '33',
        'blue': '34',
        'magenta': '35',
        'cyan': '36',
        'white': '37',
        'crimson': '38',
    }
    attrs = [shell_colors[color]]
    if bold:
        attrs.append('1')
    return '\x1b[{}m{}\x1b[0m'.format(';'.join(attrs), msg)


def exit_with_msg(is_success: bool) -> None:
    if is_success:
        print(colorize('Success!', 'green', True))
        sys.exit(0)
    else:
        print(colorize('Errors present. Check output.', 'red', True))
        sys.exit(1)


def all_files(directory: str, filter_func=None):
    for root, _, fs in os.walk(directory):
        for f in fs:
            full_path = path.join(root, f)
            if filter_func:
                if not filter_func(full_path):
                    continue
            yield full_path


def sign_file(gpg_key: str, file_: str) -> None:
    print(colorize(f'Signing {file_}', 'cyan'))
    subprocess.check_call(
        ['gpg2', '--local-user', gpg_key, '--batch', '--quiet', '--detach-sign', file_])


def check_file(file_name: str, checks: dict) -> bool:
    is_success = True

    with open(file_name) as f:
        for line_num, line in enumerate(f, start=1):
            for search, err_msg in checks.items():
                if search in line:

                    msg = colorize(f'{file_name}: {err_msg}\n', 'red', True)
                    msg += colorize(f'Line {line_num}:\n', 'white', True)
                    msg += line
                    print(msg)
                    is_success = False

    return is_success


def check(args) -> None:
    print(colorize(f'Checking file: {args.doc_name}', 'white', True))

    is_success = True
    is_success &= check_file(f'{args.doc_name}.log', LOG_CHECKS)
    is_success &= check_file(f'{args.doc_name}.blg', BLG_CHECKS)

    exit_with_msg(is_success)


def unique_list(items: list) -> list:
    out = list()
    for item in items:
        if item not in out:
            out.append(item)
    return out


def check_footnotes(tex: str) -> bool:
    is_success = True
    footnote_marks = list()
    footnote_texts = list()

    with open(tex) as f:
        for line in f:
            for t in FOOTNOTE_MARK_RE.findall(line):
                footnote_marks.append(int(t))

            for m in FOOTNOTE_TEXT_RE.findall(line):
                footnote_texts.append(int(m))

    if not footnote_marks and not footnote_texts:
        return True

    sorted_marks = list(sorted(footnote_marks))
    if sorted_marks != footnote_marks:
        print(colorize(f'Out of order footnote marks: {footnote_marks}', 'red'))
        is_success = False

    sorted_texts = list(sorted(footnote_texts))
    if sorted_texts != footnote_texts:
        print(colorize(f'Out of order footnote texts: {footnote_texts}', 'red'))
        is_success = False

    expected_range = list(range(1, 1 + len(footnote_texts)))
    if footnote_texts != expected_range:
        print(colorize(('Missing footnotetext number:\n'
                        f'  Expected: {expected_range}\n'
                        f'  Found:    {footnote_texts}'),
                       'red'))
        is_success = False

    unique_marks = unique_list(footnote_marks)
    sorted_unique = list(sorted(unique_marks))
    if unique_marks != sorted_unique:
        print(colorize(('Out of order footnotemarks:\n'
                        f'  Expected: {sorted_unique}\n'
                        f'  Found:    {unique_marks}'),
                       'red'))
        is_success = False

    mark_set = set(footnote_marks)
    text_set = set(footnote_texts)
    if mark_set != text_set:
        print(colorize(('Footnote marks does not equal texts:\n'
                        f'  Marks: {list(mark_set)}\n'
                        f'  Texts: {list(text_set)}'),
                       'red'))
        is_success = False

    if not is_success:
        print(colorize(f'Failure for file {tex}', 'red', bold=True))
    return is_success


def lint(args) -> None:
    print(colorize('Linting files', 'green', bold=True))
    is_success = True

    for f in all_files(ROOT_DIR, lambda f: f.endswith('.tex')):
        is_success &= check_footnotes(f)

    exit_with_msg(is_success)


def release(args) -> None:
    print(colorize('Generating signatures.', 'white', True))
    directory = args.directory
    os.chdir(directory)

    files = list(all_files('.'))
    for f in files:
        if f.endswith('.sig'):
            if args.delete:
                os.remove(f)
            continue

        if f'{f}.sig' not in files:
            sign_file(args.gpg_key, f)

    print(colorize('Done.', 'green', True))


def show_index(args) -> None:
    indexes = set()

    if args.file:
        files = args.file
    else:
        files = all_files(ROOT_DIR, lambda f: f.endswith('.tex'))

    for f in files:
        with open(f) as f:
            data = f.read()
            for i in INDEX_RE.findall(data):
                indexes.add(i)

    for index in sorted(indexes):
        print(index)


def index_entries(args) -> None:
    if args.file:
        files = args.file
    else:
        files = all_files(ROOT_DIR, lambda f: f.endswith('.tex'))

    output = []
    for f in files:
        with open(f) as f:
            data = f.read()
            filename = path.basename(f.name)
            indexes = len([x for x in INDEX_RE.findall(data)
                           if '|see ' not in x and '|seealso ' not in x])
            words = len(TOKENIZE_RE.split(data))
            ratio = round(indexes / float(words), 3)
            output.append([filename, str(indexes), str(words), str(ratio)])

    # sort by ratio descending
    output = sorted(output, key=lambda x: (-1 * float(x[3]), x[0]))

    max_lens = [0, 0, 0, 0]
    for row in output:
        for i, col in enumerate(row):
            max_lens[i] = max(max_lens[i], len(col))

    for row in output:
        row_str = ''
        for i, col in enumerate(row):
            row_str += col.ljust(max_lens[i])
            row_str += '  '
        print(row_str)


def tex_file(s):
    if not path.exists(s):
        raise ValueError('Path does not exist')
    if not path.isfile(s):
        raise ValueError('Path is not a file')
    if not s.endswith('.tex'):
        raise ValueError('Path is not a .tex file')
    return s


def arg_parser() -> ArgumentParser:
    parser = ArgumentParser(os.path.basename(__file__), description='Helper for dev tasks')

    subparsers = parser.add_subparsers()
    subparsers.dest = 'command'
    subparsers.required = True

    sub = subparsers.add_parser('check', help='Check the Latex output')
    sub.add_argument('doc_name', help='Latex file name (without extension)')
    sub.set_defaults(func=check)

    sub = subparsers.add_parser('lint', help='Run linters on the Latex source')
    sub.set_defaults(func=lint)

    sub = subparsers.add_parser('release', help='Create checksums and sign')
    sub.add_argument('directory', help='Directory to the files to sign')
    sub.add_argument('--delete', help='Delete old signatures first', action='store_true')
    sub.add_argument('--gpg-key', help='GPG signing key fingerprint',
                     default='9D43E6D5AF7B683CB8F8D6896F0EF79BCAF06536')
    sub.set_defaults(func=release)

    sub = subparsers.add_parser('show-index', help='extract index markers for review')
    sub.add_argument('file', help='files to check', type=tex_file, nargs='*')
    sub.set_defaults(func=show_index)

    sub = subparsers.add_parser('index-entries', help='count the number of index entries')
    sub.add_argument('file', help='files to check', type=tex_file, nargs='*')
    sub.set_defaults(func=index_entries)

    return parser


def main() -> None:
    args = arg_parser().parse_args()
    try:
        args.func(args)
    except KeyboardInterrupt:
        print('')
        sys.exit(1)
    except BrokenPipeError:
        sys.exit(1)


if __name__ == '__main__':
    main()
