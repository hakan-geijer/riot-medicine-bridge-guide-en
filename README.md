# riot-medicine-bridge-guide-en

This is a short guide designed for medical professionals to help them understand the differences between street medicine and clinical medicine.

## License

This guide and all files, documents, and source code contained in this repository is available under the Creative Commons Zero 1.0 Universal license.
Effectively, this project is public domain.
Some illustrations are not public domain, and those can be found in [`content/copyright.tex`](./content/copyright.tex).
